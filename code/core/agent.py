from abc import ABCMeta, abstractmethod
from typing import Any, Dict, List, Tuple, Union

import numpy as np
from d3rlpy.algos.base import AlgoBase
from stable_baselines3.common.base_class import BaseAlgorithm

from .data_source import DataSource, SourcedMixtureD3RLpyDataSource


class Agent(metaclass=ABCMeta):
	@abstractmethod
	def select_greedy_action(
		self, 
		states: np.ndarray
		) -> np.ndarray:		
		
		pass

	@abstractmethod
	def predict_value(
		self, 
		states: np.ndarray, 
		actions: np.ndarray,
		with_std: bool = False
		) -> Union[np.ndarray, Tuple[np.ndarray, np.ndarray]]:
		
		pass


	def train(
		self,
		training_data: DataSource
		) -> None:

		pass


	def save_model(
		self,
		model_path: str
		) -> None:

		pass


	def load_model(
		self,
		model_path: str
		) -> None:

		pass


class VacuumAgent(Agent):
	""" a placeholder class """
	def __init__(self, **kwargs):

		pass
		

	def select_greedy_action(
		self, 
		states: np.ndarray
		) -> np.ndarray:		
		raise Exception('Vacuum agent does not select actions.')


	def predict_value(
		self, 
		states: np.ndarray, 
		actions: np.ndarray,
		with_std: bool = False
		) -> Union[np.ndarray, Tuple[np.ndarray, np.ndarray]]:
		raise Exception('Vacuum agent does not predict values.')


class D3RLpyAgent(Agent):
	""" a wrapper class for d3rlpy algorithms """

	algo: AlgoBase
	algo_training_kwargs: Dict[Any, Any]

	def __init__(self, d3rlpy_algo: AlgoBase, **kwargs):
		self.algo = d3rlpy_algo
		self.algo_training_kwargs = kwargs

	def select_greedy_action(
		self, 
		states: np.ndarray
		) -> np.ndarray:
		return self.algo.predict(states)


	def predict_value(
		self, 
		states: np.ndarray, 
		actions: np.ndarray,
		with_std: bool = False
		) -> Union[np.ndarray, Tuple[np.ndarray, np.ndarray]]:
		return self.algo.predict_value(states, actions, with_std)


	def train(self, training_data: DataSource) -> None:
		transitions = training_data.get_transitions()
		assert isinstance(transitions, List), 'D3RLpyAgent only accepts list of transitions'

		# TODO: special treatment (better ways to do it)
		if isinstance(training_data, SourcedMixtureD3RLpyDataSource):
			setter_fn = getattr(self.algo, 'set_data_source', None)
			if setter_fn is not None:
				setter_fn(training_data)

		self.algo.fit(transitions, **self.algo_training_kwargs)


	def save_model(
		self,
		model_path: str
		) -> None:
		self.algo.save_model(fname=model_path)


	def load_model(
		self,
		model_path: str
		) -> None:
		self.algo.load_model(fname=model_path)


class StableBaselinesAgent(Agent):
	""" a wrapper class for stable baselines3 """

	algo: BaseAlgorithm

	def __init__(self, stable_baselines_algo: BaseAlgorithm, model_path: str, **kwargs):
		self.algo = stable_baselines_algo
		self.algo = self.algo.load(model_path)


	def select_greedy_action(
		self, 
		states: np.ndarray
		) -> np.ndarray:
		return self.algo.predict(states, deterministic=True)[0]


	def predict_value(
		self, 
		states: np.ndarray, 
		actions: np.ndarray,
		with_std: bool = False
		) -> Union[np.ndarray, Tuple[np.ndarray, np.ndarray]]:
		raise Exception('Stable baseline3 models currently do not support value predictions.')


	def save_model(
		self,
		model_path: str
		) -> None:
		self.algo.save(model_path)


	def load_model(
		self,
		model_path: str
		) -> None:
		self.algo = self.algo.load(model_path)
		

