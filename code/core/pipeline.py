from abc import ABCMeta, abstractmethod

from .agent import Agent
from .data_source import DataSource
from .online_strategy import OnlineStrategy


class Pipeline(metaclass=ABCMeta):
	@abstractmethod
	def process(self) -> Agent:
		pass


class SingleStagePipeline(Pipeline):
	""" Standard offline training pipeline with single agent and single datasource """
	offline_data: DataSource
	agent: Agent

	def __init__(self, offline_data: DataSource, agent: Agent):
		self.offline_data = offline_data
		self.agent = agent

	def process(self) -> Agent:
		self.agent.train(self.offline_data)
		return self.agent


class TwoStagePipeline(Pipeline):
	""" Two stage training pipeline with two (or one) agents, a single datasource, and a online strategy """
	offline_data: DataSource
	first_stage_agent: Agent
	online_strategy: OnlineStrategy
	second_stage_agent: Agent

	def __init__(self,
		offline_data: DataSource,
		first_stage_agent: Agent,
		online_strategy: OnlineStrategy,
		second_stage_agent: Agent
		):
		
		self.offline_data = offline_data
		self.first_stage_agent = first_stage_agent
		self.online_strategy = online_strategy
		self.second_stage_agent = second_stage_agent

	def process(self) -> Agent:
		# first train an agent that can interact with the online env
		self.first_stage_agent.train(self.offline_data)
		# use the trained agent to interact with the environment
		online_data = self.online_strategy.collect_data(self.first_stage_agent)
		# combine the offline data with the newly collected online data
		combined_data = self.offline_data.add_data(online_data)
		# train an agent with all the data
		self.second_stage_agent.train(combined_data)

		return self.second_stage_agent
