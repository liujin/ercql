from abc import abstractmethod
from collections import defaultdict
from typing import Any, Dict, List, Optional, Union

from d3rlpy.dataset import MDPDataset, Transition
from dataset.data_generation import get_dataset  # type: ignore
from dataset.data_utils import (add_source_to_transitions,  # type: ignore
                                convert_to_transitions,
                                randomly_split_transitions)


class DataSource:
	@abstractmethod
	def get_transitions(self) -> Union[List[Transition], Dict[Any, List[Transition]]]:
		pass

	@abstractmethod
	def add_data(self, other: 'DataSource') -> 'DataSource':
		pass


class D3RLpyDataSource(DataSource):
	transitions: List[Transition]

	def __init__(self, dataset: Union[str, MDPDataset], sample_size: Optional[int] = None):
		if isinstance(dataset, str):
			dataset = get_dataset(dataset)
		transitions = convert_to_transitions(dataset)
		if sample_size:
			transitions, _ = randomly_split_transitions(transitions, sample_size)
		self.transitions = transitions.tolist()

	def get_transitions(self) -> Union[List[Transition], Dict[Any, List[Transition]]]:
		return self.transitions

	def add_data(self, other: DataSource) -> DataSource:
		assert isinstance(other, D3RLpyDataSource), 'Only the same datasource can be added'
		self.transitions += other.get_transitions()
		return self


class EmptyD3RLpyDataSource(D3RLpyDataSource):
	def __init__(self):
		self.transitions = []


class MixtureD3RLpyDataSource(D3RLpyDataSource):
	transition_dict: Dict[str, List[Transition]]
	merge_transitions: bool

	def __init__(self,
		dataset: Dict[str, Union[str, MDPDataset]],
		sample_size: Optional[List[int]] = None,
		merge_transitions: bool = False
		):

		assert len(dataset) > 0, \
			'The size of the dataset dict should be greater than zero.'
		assert sample_size is None or len(sample_size) == len(dataset), \
			'Sample_size should have the same dim as the dataset.'

		self.transition_dict = {}
		self.merge_transitions = merge_transitions

		for idx, dataset_key in enumerate(dataset):
			sub_dataset = dataset[dataset_key]
			if isinstance(sub_dataset, str):
				sub_dataset = get_dataset(sub_dataset)
			transitions = convert_to_transitions(sub_dataset)

			if sample_size:
				transitions, _ = randomly_split_transitions(
					transitions, int(sample_size[idx]))

			self.transition_dict[dataset_key] = transitions.tolist()

	def get_transitions(self) -> Union[List[Transition], Dict[Any, List[Transition]]]:
		if self.merge_transitions:
			transitions = []
			for tran in self.transition_dict.values():
				transitions += tran
			return transitions
		else:
			return self.transition_dict

	def add_data(self, other: DataSource) -> DataSource:
		raise NotImplementedError


class SourcedMixtureD3RLpyDataSource(D3RLpyDataSource):
	""" A hacky version that adds source label to the end of the obs dim """

	cur_max_source_idx: int
	source_label_cnt: Dict[int, int]

	def __init__(self,
		# restrict the dataset dict to have (source_label, dataset_name) pair
		dataset: Dict[int, str],
		sample_size: Optional[List[int]] = None
		):

		assert len(dataset) > 0, \
			'The size of the dataset dict should be greater than zero.'
		assert sample_size is None or len(sample_size) == len(dataset), \
			'Sample_size should have the same dim as the dataset.'

		self.transitions = []
		self.cur_max_source_idx = 0
		self.source_label_cnt = defaultdict(int)

		for idx, source_label in enumerate(dataset): 
			ds = get_dataset(dataset[source_label], source_label=source_label)
			transitions = convert_to_transitions(ds)

			if sample_size:
				transitions, _ = randomly_split_transitions(
					transitions, int(sample_size[idx]))

			transition_list = transitions.tolist()

			self.transitions += transition_list
			self.cur_max_source_idx = max(self.cur_max_source_idx, source_label)
			self.source_label_cnt[source_label] += len(transition_list)

	def get_label_ratio(self, label: int) -> float:
		return 1.0 * self.source_label_cnt[label] / len(self.transitions)

	def add_data(self, other: DataSource) -> DataSource:
		if isinstance(other, SourcedMixtureD3RLpyDataSource):
			self.transitions += other.get_transitions()
			for source_label, cnt in other.source_label_cnt.items():
				self.source_label_cnt[source_label] += cnt
		elif isinstance(other, D3RLpyDataSource):
			self.cur_max_source_idx += 1
			transitions = other.get_transitions()
			assert isinstance(transitions, list)
			newly_added_transitions = add_source_to_transitions(transitions, self.cur_max_source_idx)
			self.source_label_cnt[self.cur_max_source_idx] += len(newly_added_transitions)
			self.transitions += newly_added_transitions
		else:
			raise TypeError('Only SourcedMixtureD3RLpyDataSource or D3RLpyDataSource is acceptable.')
		return self
