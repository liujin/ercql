from abc import ABCMeta, abstractmethod
from typing import Optional, Union

import gym  # type: ignore
import numpy as np
from dataset.data_generation import collect_dataset_with_policy  # type: ignore

from .agent import Agent
from .data_source import D3RLpyDataSource, DataSource, EmptyD3RLpyDataSource


class OnlineStrategy(metaclass=ABCMeta):
	env: gym.Env
	online_steps: int

	def __init__(self, env: Optional[Union[str, gym.Env]] = None, online_steps: int = 10000):
		if isinstance(env, str):
			self.env = gym.make(env)
		else:
			self.env = env
		self.online_steps = online_steps

	@abstractmethod
	def collect_data(
		self,
		agent: Agent
		) -> DataSource:

		pass


class EpsilonGreedyOnlineStrategy(OnlineStrategy):

	epsilon: float

	def __init__(
		self, 
		env: Union[str, gym.Env], 
		online_steps: int, 
		epsilon: float = 0.1,
		render: bool = False
		):
		super(EpsilonGreedyOnlineStrategy, self).__init__(env, online_steps)
		self.epsilon = epsilon
		self.render = render

	def collect_data(
		self,
		agent: Agent
		) -> DataSource:

		def policy(state: np.ndarray, t_idx: int) -> int:
			if np.random.rand() < self.epsilon:
				return self.env.action_space.sample()
			else:
				return agent.select_greedy_action(np.expand_dims(state, axis=0))[0]

		return D3RLpyDataSource(collect_dataset_with_policy(
			self.env, policy, self.online_steps, render=self.render))


class UCBOnlineStrategy(OnlineStrategy):

	std_multiplier: float

	def __init__(
		self, 
		env: Union[str, gym.Env], 
		online_steps: int, 
		std_multiplier: float = 1,
		render: bool = False
		):
		super(UCBOnlineStrategy, self).__init__(env, online_steps)
		self.std_multiplier = std_multiplier
		self.render = render


	def collect_data(
		self,
		agent: Agent
		) -> DataSource:

		stacked_actions = np.arange(self.env.action_space.n).reshape(-1, 1)

		def policy(state: np.ndarray, t_idx: int) -> int:
			stacked_states = np.tile(state, (self.env.action_space.n, 1))
			q_values, q_stds = agent.predict_value(stacked_states, stacked_actions, with_std=True)
			ucbs = q_values + q_stds * self.std_multiplier
			return ucbs.argmax()

		return D3RLpyDataSource(collect_dataset_with_policy(
			self.env, policy, self.online_steps, render=self.render))


class EmptyOnlineStrategy(OnlineStrategy):
	def __init__(self):
		super(EmptyOnlineStrategy, self).__init__()

	def collect_data(
		self,
		agent: Agent
		) -> DataSource:
		return EmptyD3RLpyDataSource()

