import argparse
from os import listdir

from visualization.data_plot import (plot_records_in_dir,
                                     print_final_performance_in_dir)


def parse_experiment_name(name):
	parts = name.split('_')
	# takes the form ${pipeline_name}_${seed}_${timestamp}
	return ('_'.join(parts[:-2]), ''.join(parts[-2:]))

LOG_DIR = './d3rlpy_logs'

if __name__ == '__main__':
	parser = argparse.ArgumentParser(description='Plot performances')
	parser.add_argument('-p', '--print', action='store_true')
	parser.add_argument('-n', '--experiment-name', default='', type=str)
	parser.add_argument('-e', '--env-name', default='LunarLander-v2', type=str)
	parser.add_argument('-v', '--value-description', default='rollout_return', type=str)
	parser.add_argument('-H', '--horizon-idx', default=-1, type=int)
	args = parser.parse_args()

	if len(args.experiment_name) == 0:
		for f in listdir(LOG_DIR):
			args.experiment_name = f'{LOG_DIR}/{f}'
			break
		print(f'Use the first folder in the log dir: {args.experiment_name}')

	if args.print:
		print_final_performance_in_dir(
			log_dir=args.experiment_name,
			value_description=args.value_description,
			horizon_idx=args.horizon_idx,
			parse_experiment_name=parse_experiment_name
		)
	else:
		plot_records_in_dir(
			log_dir=args.experiment_name,
			env_name=args.env_name,
			value_description=args.value_description,
			parse_experiment_name=parse_experiment_name
			# normalize_baseline='behaviour_greedy_cql'
		)
