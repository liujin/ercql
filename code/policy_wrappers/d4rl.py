"""
	Extract policy from meta information
"""
from typing import Any, Dict, Tuple, Union

import gym  # type: ignore
import numpy as np
import torch
import torch.nn as nn
from torch.distributions import Normal, Uniform

import d4rl # type: ignore

from .wrappers import LoggingPolicy


def _load_linear(weight: np.ndarray, bias: np.ndarray) -> nn.Linear:
	(out_d, in_d) = weight.shape
	linear = nn.Linear(out_d, in_d)
	with torch.no_grad():
		linear.weight.data = torch.from_numpy(weight)
		linear.bias.data = torch.from_numpy(bias)
		return linear


def _extract_policy(info_dict: Dict[str, Any]) -> nn.Module:
	
	class D4RLPolicy(nn.Module):
		def __init__(self) -> None:
			super(D4RLPolicy, self).__init__()
			nonlinearity = nn.Tanh if info_dict['metadata/policy/nonlinearity'] == b'tanh' else nn.ReLU
			self.common_base = nn.Sequential(
				_load_linear(
					info_dict['metadata/policy/fc0/weight'], 
					info_dict['metadata/policy/fc0/bias']),
				nonlinearity(),
				_load_linear(
					info_dict['metadata/policy/fc1/weight'], 
					info_dict['metadata/policy/fc1/bias']),
				nonlinearity()
			)
			self.mean_head = _load_linear(
				info_dict['metadata/policy/last_fc/weight'], 
				info_dict['metadata/policy/last_fc/bias'])
			self.log_std_head = _load_linear(
				info_dict['metadata/policy/last_fc_log_std/weight'], 
				info_dict['metadata/policy/last_fc_log_std/bias'])


		def forward(self, states: torch.Tensor) -> Tuple[torch.Tensor, torch.Tensor]:
			base = self.common_base(states)
			mean = self.mean_head(base)
			log_std = self.log_std_head(base)
			return mean, log_std
	
	return D4RLPolicy()


class TanhNormal(Normal):
	""" be careful when querying other info about this """
	def sample(self, sample_shape: torch.Size = torch.Size()) -> torch.Tensor:
		return torch.tanh(super().sample(sample_shape))

	@property
	def mean(self):
		return torch.tanh(super().mean)


class D4RLBulletMujocoLoggingPolicy(LoggingPolicy):
	def __init__(self, task_name: str, device: str) -> None:
		self._device = torch.device(device)

		if 'random' in task_name:
			self._random = True
			self._action_space = gym.make(task_name).action_space
		else:
			self._random = False
			# dataset contains info about the policy weights
			dataset = gym.make(task_name).get_dataset()
			self._policy = _extract_policy(dataset).to(self._device)


	def predict(self, obs: Union[torch.Tensor, np.ndarray]) -> torch.Tensor:
		if self._random:
			batch_size = obs.shape[0]
			""" the action spaces are already normalized """
			return torch.from_numpy(np.array([self._action_space.sample() for _ in range(batch_size)])).to(self._device)
		else:
			if isinstance(obs, np.ndarray):
				obs = torch.from_numpy(obs)
			obs.to(self._device)
			return torch.tanh(self._policy.forward(obs)[0])


	def get_action_pmf(self, obs: Union[torch.Tensor, np.ndarray]) -> torch.Tensor:
		raise NotImplementedError('D4rl bullet mujoco tasks are continuous')


	def get_action_pdf(
		self, 
		obs: Union[torch.Tensor, np.ndarray]
	) -> torch.distributions.Distribution:
		if self._random:
			batch_size = obs.shape[0]
			low = self._action_space.low
			high = self._action_space.high

			low_tensor = torch.from_numpy(np.tile(low, (batch_size, 1))).to(self._device)
			high_tensor = torch.from_numpy(np.tile(high, (batch_size, 1))).to(self._device)
			return Uniform(low_tensor, high_tensor)
		else:
			if isinstance(obs, np.ndarray):
				obs = torch.from_numpy(obs)
			obs.to(self._device)
			mean, log_std = self._policy.forward(obs)
			return TanhNormal(mean, torch.exp(log_std))
