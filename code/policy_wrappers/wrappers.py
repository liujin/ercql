from abc import ABCMeta, abstractmethod
from typing import List, Union

import numpy as np
import torch
from stable_baselines3 import PPO


def load_logging_policy(policy_name: str, saving_dir: str = './d3rlpy_data', device: str = 'cpu'):
	# TODO: later change to more elegant manner
	if policy_name.startswith('ThreeState-v0'):
		return ThreeStateLoggingPolicy(policy_name)
	if policy_name.startswith(('hopper', 'halfcheetah', 'walker2d')):
		from .d4rl import D4RLBulletMujocoLoggingPolicy
		return D4RLBulletMujocoLoggingPolicy(task_name=policy_name, device=device)
	return PPOLoggingPolicy(policy_name=policy_name, saving_dir=saving_dir, device=device)


class LoggingPolicy(metaclass=ABCMeta):
	@abstractmethod
	def predict(self, obs: Union[torch.Tensor, np.ndarray]) -> torch.Tensor:
		pass

	@abstractmethod
	def get_action_pmf(self, obs: Union[torch.Tensor, np.ndarray]) -> torch.Tensor:
		pass

	@abstractmethod
	def get_action_pdf(self, obs: Union[torch.Tensor, np.ndarray]) -> torch.distributions.Distribution:
		pass


class PPOLoggingPolicy(LoggingPolicy):
	def __init__(self, policy_name: str, saving_dir: str, device: str):
		self._policy = PPO.load(f'{saving_dir}/{policy_name}.zip', device=device)


	def predict(self, obs: Union[torch.Tensor, np.ndarray]) -> torch.Tensor:
		assert self._policy.policy is not None
		if isinstance(obs, torch.Tensor):
			obs = obs.numpy()
		greedy_actions, _ = self._policy.predict(obs, deterministic=True)
		return greedy_actions


	def get_action_pmf(self, obs: Union[torch.Tensor, np.ndarray]) -> torch.Tensor:
		assert self._policy.policy is not None
		if isinstance(obs, np.ndarray):
			obs = torch.FloatTensor(obs)
		return self._policy.policy.get_distribution(obs).distribution.probs


	def get_action_pdf(self, obs: Union[torch.Tensor, np.ndarray]) -> torch.distributions.Distribution:
		assert self._policy.policy is not None
		if isinstance(obs, np.ndarray):
			obs = torch.FloatTensor(obs)
		return self._policy.policy.get_distribution(obs).distribution


class D4RLLoggingPolicy(LoggingPolicy):
	def __init__(self, policy_name: str, saving_dir: str):
		pass


class CustomDefinedLoggingPolicy(LoggingPolicy):
	_obs_dim: int

	@abstractmethod
	def _single_predict(self, sample: np.ndarray) -> int:
		pass


	@abstractmethod
	def _single_pmf(self, sample: np.ndarray) -> List[float]:
		pass


	def _prepare_obs(self, obs: Union[torch.Tensor, np.ndarray]) -> np.ndarray:
		if isinstance(obs, torch.Tensor):
			obs = obs.numpy()
		assert isinstance(obs, np.ndarray)
		return obs.reshape(-1, self._obs_dim)


	def predict(self, obs: Union[torch.Tensor, np.ndarray]) -> torch.Tensor:
		return torch.LongTensor(
			[self._single_predict(sample) for sample in self._prepare_obs(obs)])


	def get_action_pmf(self, obs: Union[torch.Tensor, np.ndarray]) -> torch.Tensor:
		return torch.FloatTensor(
			[self._single_pmf(sample) for sample in self._prepare_obs(obs)])


	def get_action_pdf(self, obs: Union[torch.Tensor, np.ndarray]) -> torch.distributions.Distribution:
		raise NotImplementedError


class ThreeStateLoggingPolicy(CustomDefinedLoggingPolicy):
	_obs_dim: int = 1

	def __init__(self, policy_name: str):
		self._policy_idx = int(policy_name.split('_')[1])


	def _single_predict(self, sample: np.ndarray) -> int:
		return self._policy_idx


	def _single_pmf(self, sample: np.ndarray) -> List[float]:
		action_probs = [0.0] * 2
		action_probs[self._policy_idx] = 1.0
		return action_probs

