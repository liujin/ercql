import os
from typing import Any, Callable, List, Optional, Tuple, Union

import gym  # type: ignore
import numpy as np
from d3rlpy.dataset import Episode, MDPDataset, Transition
from gym.spaces import Box, Discrete  # type: ignore
from stable_baselines3 import DQN, PPO
from stable_baselines3.common.base_class import BaseAlgorithm
from stable_baselines3.common.evaluation import evaluate_policy

from .data_utils import add_source_to_dataset, convert_to_transitions


def get_dataset(
	dataset_name: str, 
	dataset_path: str = './d3rlpy_data', 
	source_label: Optional[int] = None
	) -> MDPDataset:

	try:
		from d3rlpy.datasets import get_dataset as d3rlpy_get_dataset
		ret_dataset = d3rlpy_get_dataset(dataset_name)[0]
	except:
		print('Unable to find dataset from d3rlpy...')
		print('Loading data from local...')

		dataset_path = os.path.join(dataset_path, dataset_name + '.h5')
		assert os.path.exists(dataset_path), f'Dataset {dataset_name} does not exist'

		ret_dataset = MDPDataset.load(dataset_path)

	if isinstance(source_label, int) and source_label >= 0:
		ret_dataset = add_source_to_dataset(ret_dataset, source_label)

	return ret_dataset


def collect_dataset_with_policy(
	env: gym.Env, 
	policy: Callable[[np.ndarray, int], Any], 
	num_of_steps: int,
	max_horizon: int = 2000,
	render: bool = False,
	pause_before_episode: bool = False
	) -> MDPDataset:
	""" Works with single dimension action (both discrete and continuous) and state spaces """

	assert isinstance(env.observation_space, Box), 'The observation space must be a Box'
	is_discrete_action = isinstance(env.action_space, Discrete)

	# if we pause before episode, render is set to True automatically
	render = render or pause_before_episode

	actions = np.zeros((0, 1), dtype=np.int32) if is_discrete_action \
		else np.zeros((0, env.action_space.shape[0]), dtype=np.float32)
	observations = np.zeros((0, env.observation_space.shape[0]), dtype=np.float32)
	rewards = np.zeros(0, dtype=np.float32)
	terminals = np.zeros(0, dtype=np.float32)

	while len(actions) < num_of_steps:
		obs = env.reset()
		cumulative_rewards = 0
		epsisode_len = 0

		for t_idx in range(max_horizon):
			action = policy(obs, t_idx)
			nxt_obs, reward, done, _ = env.step(action)

			observations = np.append(observations, [obs], axis=0)
			actions = np.append(actions, [[action]] if is_discrete_action else [action], axis=0)
			rewards = np.append(rewards, reward)
			terminals = np.append(terminals, done)

			obs = nxt_obs
			cumulative_rewards += reward
			epsisode_len += 1

			if render:
				env.render()

			# we do not directly quit here because we need the full information
			if done:
				break

		if render:
			print(f'Episode len: {epsisode_len}, reward: {cumulative_rewards}')

	return MDPDataset(
		observations=observations[:num_of_steps],
		actions=actions[:num_of_steps],
		rewards=rewards[:num_of_steps],
		terminals=np.append(terminals[:num_of_steps - 1], 1.0), # make sure it ends with 1.0
		discrete_action=is_discrete_action
	)


def collect_transitions_with_policy(
	env: gym.Env, 
	policy: Callable[[np.ndarray, int], Any], 
	num_of_steps: int,
	max_horizon: int = 2000
	) -> np.ndarray:
	return convert_to_transitions(
		collect_dataset_with_policy(
			env=env, 
			policy=policy, 
			num_of_steps=num_of_steps, 
			max_horizon=max_horizon
		))


def get_baseline_agent(
	env_name: str,
	model_name: str = 'replay',
	model_train_steps: int = int(8e4),
	data_dir: str = './d3rlpy_data',
	create_new_model: bool = False
	) -> BaseAlgorithm:

	model_path = os.path.join(data_dir, 
		'{}_{}.zip'.format(env_name, model_name))

	if os.path.exists(model_path) and not create_new_model:
		print('Loading model from {}'.format(model_path))
		model = PPO.load(model_path)
	else:
		print('Learning and save model to {}'.format(model_path))
		model = PPO(
			'MlpPolicy', 
			env_name, 
			gamma=0.98,
			# only for continous action space environment
			# use_sde=True,
			# sde_sample_freq=4,
			learning_rate=1e-3,
			verbose=1).learn(model_train_steps)
		model.save(model_path)
	return model


def generate_dataset(
	env_name: str,
	max_horizon: int = 2000,
	model_name: str = 'replay',
	model_train_steps: int = int(8e4),
	dataset_name: Optional[str] = None,
	dataset_size: int = 100000,
	data_dir: str = './d3rlpy_data',
	create_new_model: bool = False
	) -> None:
	
	model = get_baseline_agent(
		env_name=env_name,
		model_name=model_name,
		model_train_steps=model_train_steps,
		data_dir=data_dir,
		create_new_model=create_new_model)

	env = gym.make(env_name)

	dataset = collect_dataset_with_policy(
		env=env,
		policy=lambda obs, _: model.predict(obs, deterministic=True)[0],
		num_of_steps=dataset_size,
		max_horizon=max_horizon
	)

	print('Finished generating dataset: mean return = {:.2f}'.format(
		dataset.compute_stats()['return']['mean']))

	print('Evaluate policy: mean return = {:.2f}, std = {:.2f}'.format(
		*evaluate_policy(model, env, n_eval_episodes=10, deterministic=True)))

	dataset_path = os.path.join(data_dir,
		'{}_{}.h5'.format(
			env_name, dataset_name or model_name
		)
	)
	dataset.dump(dataset_path)


def generate_epsilon_greedy_dataset(
	env_name: str,
	max_horizon: int = 2000,
	model_name: str = 'replay',
	model_train_steps: int = int(8e4),
	dataset_name: Optional[str] = None,
	dataset_size: int = 100000,
	data_dir: str = './d3rlpy_data',
	create_new_model: bool = False,
	# epsilon greedy parameters
	epsilon: float = 0.2,
	interval: Optional[Tuple[int, int]] = None
	) -> None:
	
	model = get_baseline_agent(
		env_name=env_name,
		model_name=model_name,
		model_train_steps=model_train_steps,
		data_dir=data_dir,
		create_new_model=create_new_model)

	env = gym.make(env_name)

	# revise interval
	if interval is not None:
		left = max(0, interval[0])
		right = max_horizon if interval[1] == -1 else min(interval[1], max_horizon)
		interval = (left, right)

	def epsilon_greedy_policy(obs: np.ndarray, t_idx: int) -> Union[int, np.ndarray]:
		if interval is None or interval[0] <= t_idx <= interval[1]:
			if np.random.rand() < epsilon:
				return env.action_space.sample()	
		return model.predict(obs, deterministic=True)[0]

	dataset = collect_dataset_with_policy(
		env=env,
		policy=epsilon_greedy_policy,
		num_of_steps=dataset_size,
		max_horizon=max_horizon
	)

	print('Finished generating dataset: mean return = {:.2f}'.format(
		dataset.compute_stats()['return']['mean']))

	print('Evaluate policy: mean return = {:.2f}, std = {:.2f}'.format(
		*evaluate_policy(model, env, n_eval_episodes=10, deterministic=True)))

	interval_str = 'all'
	if interval is not None:
		interval_str = '{}-{}'.format(interval[0], interval[1])

	dataset_path = os.path.join(data_dir,
		'{}_{}_eps_{}_{}.h5'.format(
			env_name, 
			dataset_name or model_name, 
			str(epsilon).replace('.', 'p'),
			interval_str
		)
	)
	dataset.dump(dataset_path)

