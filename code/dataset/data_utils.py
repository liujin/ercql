import os

import numpy as np

from typing import List, Optional, Tuple

from d3rlpy.dataset import MDPDataset, Episode, Transition


def add_source_to_transitions(
	transitions: List[Transition], 
	source_label: int = 0
	) -> List[Transition]:

	for idx in range(len(transitions)):
		cur_tran = transitions[idx]

		padded_obs = np.r_[cur_tran.observation, [source_label]]
		padded_nxt_obs = np.r_[cur_tran.next_observation, [source_label]]

		transitions[idx] = Transition(
			observation_shape=padded_obs.shape,
			action_size=cur_tran.get_action_size(),
			observation=padded_obs,
			action=cur_tran.action,
			reward=cur_tran.reward,
			next_observation=padded_nxt_obs,
			terminal=cur_tran.terminal,
			# prev_transition=cur_tran.prev_transition,
			# next_transition=cur_tran.next_transition
		)

	return transitions


def add_source_to_dataset(dataset: MDPDataset, source_label: int = 0) -> MDPDataset:
	# shape (B, OBS_DIM)
	original_obs = dataset.observations

	# padding with source_label to the last dim
	sourced_obs = np.c_[original_obs, np.ones(original_obs.shape[0]) * source_label]

	return MDPDataset(
		observations=sourced_obs,
		actions=dataset.actions,
		rewards=dataset.rewards,
		terminals=dataset.terminals,
		discrete_action=dataset.is_action_discrete()
	)


def convert_to_transitions(ds: Optional[MDPDataset]) -> np.ndarray:
	""" Adopted from d3rlpy, get transitions from a d3rlpy dataset """

	transitions = []
	
	if isinstance(ds, MDPDataset):
		for episode in ds.episodes:
			transitions += episode.transitions
	elif ds is None or len(ds) == 0:
		raise ValueError("empty dataset is not supported.")

	elif isinstance(ds[0], Episode):
		for episode in ds:
			transitions += episode.transitions
	elif isinstance(ds[0], Transition):
		transitions = list(ds)
	else:
		raise ValueError(f"invalid dataset type: {type(ds)}")

	return np.array(transitions)


def randomly_split_transitions(
	transitions: np.ndarray, 
	sample_size: int
	) -> Tuple[np.ndarray, np.ndarray]:
	"""
		Randomly split the transitions into two parts with the size of the first part being sample_size
	"""

	# sample without replacement
	all_indices = np.arange(len(transitions))

	sampled_indices = np.random.choice(all_indices, size=sample_size, replace=False)
	remaining_indices = [i for i in all_indices if i not in sampled_indices]

	assert len(all_indices) == len(sampled_indices) + len(remaining_indices)
	return transitions[sampled_indices], transitions[remaining_indices]

