import re

from typing import Any, Dict, List

from collections import OrderedDict
from omegaconf import OmegaConf

GEN_FILE_NAME = './code/conf/pipelines/single_stage/sweep_ercql_arch_test_1.yaml'
BASE_INSTANCE = 'ercql'

config: Dict[Any, Any] = {}
base_params: Dict[str, Any] = {
	'offline_data/dataset': {
		0: '${experiment.replay_dataset_name}',
		1: '${experiment.suboptimal_dataset_name}'
	},
	'offline_data/sample_size': ['${experiment.offline_datasize}', 500]
}

sweep_params: Dict[str, Any] = {
	'agent/d3rlpy_algo/encoder_factory/hidden_units': ['${as_np:[256, 256]}', '${as_np:[512, 512]}'],
	'agent/d3rlpy_algo/batch_size': [32, 64, 128],
	'agent/d3rlpy_algo/learning_rate': [5e-5, 1e-4, 5e-4]
}

# add sweep_params and define names
value_list: List[Any] = [[]]
for arg_values in sweep_params.values():
	new_list = []
	for value in arg_values:
		for cur_values in value_list:
			new_list.append(cur_values + [value])
	value_list = new_list

def add_config(mp, key_list, value, idx = 0):
	if idx == len(key_list) - 1:
		mp[key_list[-1]] = value
	else:
		if key_list[idx] not in mp:
			mp[key_list[idx]] = {}
		add_config(mp[key_list[idx]], key_list, value, idx + 1)

def transform_value(v):
	if isinstance(v, bool):
		return 'y' if v else 'n'
	elif isinstance(v, int):
		return v
	elif isinstance(v, float):
		float_str = f'{v:.2f}'
		if float(float_str) == 0:
			# use scientific notation for very small float
			float_str = f'{v:.2e}'
		p_idx = float_str.index('.')
		return float_str[:p_idx] + 'p' + float_str[p_idx+1:]
	elif isinstance(v, str):
		return re.sub(r'\W+', '', v)[-3:]
	elif v is None:
		return 'none'
	else:
		raise NotImplementedError

instance_names = []
for param_values in value_list:
	instance_name = []
	instance_config: Dict[str, Any] = {}
	param_names = list(sweep_params.keys())
	
	for name, value in zip(param_names, param_values):
		name_path = name.split('/')
		first_letter_name = ''.join([x[0] for x in name_path[-1].split('_')])
		instance_name.append(f'{first_letter_name}={transform_value(value)}')
		add_config(instance_config, name_path, value)
	
	# add base params
	for name, value in base_params.items():
		name_path = name.split('/')
		add_config(instance_config, name_path, value)

	instance_names.append('_'.join(instance_name))
	config[instance_names[-1]] = instance_config

# make the 'defaults' the first
final_config: Dict[str, Any] = dict(defaults=[])
for name in instance_names:
	final_config['defaults'].append({f'instances@{name}': BASE_INSTANCE})
final_config.update(config)
final_omega_config = OmegaConf.create(final_config)

print(OmegaConf.to_yaml(final_omega_config))
OmegaConf.save(final_omega_config, GEN_FILE_NAME)

def line_prepender(filename, line):
	with open(filename, 'r+') as f:
		content = f.read()
		f.seek(0, 0)
		f.write(line.rstrip('\r\n') + '\n' + content)

# this is required
line_prepender(GEN_FILE_NAME, '# @package pipelines\n')

