import gym # type: ignore

def register_custom_env():
	gym.envs.register(
		id='BridgeGridWorld-v0', 
		entry_point='custom_envs.bridge_grid_world:BridgeGridWorld',
		max_episode_steps=100)
	gym.envs.register(
		id='ThreeState-v0',
		entry_point='custom_envs.three_state:ThreeState'
		)

register_custom_env()

if __name__ == '__main__':
	print(gym.make('BridgeGridWorld-v0'))