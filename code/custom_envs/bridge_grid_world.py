from typing import Tuple

import gym  # type: ignore
import numpy as np


class BridgeGridWorld(gym.Env):
	FORWARD = [1, 0]
	BACKWARD = [-1, 0]
	LEFT = [0, -1]
	RIGHT = [0, 1]

	def __init__(self):
		self._grid = np.array([
			[0, 0, 0, 0, 0, 0, 0],
			[0, 0, 0, 0, 0, 0, 0],
			[0, 0, 0, 0, 0, 0, 0],
			[0, 0, 0, 0, 0, 0, 0],
			[0, 0, 0, 0, 0, 0, 0],
			[1, 1, 1, 0, 1, 1, 1],
			[0, 0, 0, 0, 0, 0, 0],
			[0, 0, 0, 0, 0, 0, 0],
			[0, 0, 0, 0, 0, 0, 0],
			[0, 0, 0, 0, 0, 0, 0],
			[0, 0, 0, 0, 0, 0, 0]
		])
		self._height = self._grid.shape[0]
		self._width = self._grid.shape[1]

		self._dir = np.array([self.RIGHT, self.LEFT, self.BACKWARD, self.FORWARD])
		self._start_point = np.array([0, self._width // 2])
		self._end_point = np.array([self._height - 1, self._width // 2])

		self._step = 0
		self._max_horizon = 30
		
		self._state = None

		# required attributes for env
		self.action_space = gym.spaces.Discrete(4)
		self.observation_space = gym.spaces.Box(
			low=np.array([0, 0]),
			high=np.array([self._height - 1, self._width - 1]),
			dtype=np.int32)

		self.metadata = {}


	def reset(self) -> np.ndarray:
		self._step = 0
		self._state = self._start_point
		return self._state / 10


	def _update_state_with_action(self, action: int) -> bool:
		direction = self._dir[action]
		new_state = self._state + direction
		if 0 <= new_state[0] < self._height and 0 <= new_state[1] < self._width:
			if self._grid[tuple(new_state)] == 0:
				self._state = new_state
				return True
		return False


	def step(self, action: int) -> Tuple[np.ndarray, float, bool, dict]:
		assert self._state is not None, "Call reset before calling step"
		assert self._step < self._max_horizon, "Call reset after exceeding max horizon"
		assert self.action_space.contains(action), f"{action!r} ({type(action)}) invalid"
		
		# increment step
		self._step += 1

		# check step validity
		valid = self._update_state_with_action(action)

		# get reward
		end = False
		reward = -1 if valid else -3
		if valid and np.array_equal(self._state, self._end_point):
			reward = 15
			end = True

		return self._state / 10, reward, end or self._step >= self._max_horizon, {}


	def render(self, mode='human') -> None:
		assert self._state is not None, 'Please call reset first.'
		grid = self._grid.copy().astype(object)
		grid[tuple(self._state)] = 'X'
		for row in range(self._height):
			print(grid[row].tolist())


	def close(self) -> None:
		return

