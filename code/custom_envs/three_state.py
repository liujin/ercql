from typing import List, Tuple

import gym  # type: ignore
import numpy as np


class ThreeState(gym.Env):
	_reward_scale: float = 5
	_rewards: List = [
		[1, -1],
		[-1, 1]
	]

	def __init__(self):
		self._state = 0

		# required attributes for env
		self.action_space = gym.spaces.Discrete(2)
		self.observation_space = gym.spaces.Box(
			low=np.array([0]),
			high=np.array([1]),
			dtype=np.int32)
		self.metadata = {}


	def reset(self) -> np.ndarray:
		self._state = 0
		return np.array([self._state])


	def step(self, action: int) -> Tuple[np.ndarray, float, bool, dict]:
		assert self._state < 2, f"Call reset before calling step"
		assert self.action_space.contains(action), f"{action!r} ({type(action)}) invalid"
		reward = self._rewards[self._state][action] * self._reward_scale
		self._state += 1
		return np.array([self._state]), reward, self._state == 2, {}


	def render(self, mode='human') -> None:
		return


	def close(self) -> None:
		return

