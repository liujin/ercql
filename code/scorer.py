"""
This one mimics the scorers from d3rlpy but evaluates on an environment instead:
	All of the scorers are the same as the d3rlpy.metrics.scorers except that the episodes
	are generated on policy with the agent
"""

from typing import Any, Callable, List

import gym  # type: ignore
import numpy as np
from d3rlpy.dataset import Episode, MDPDataset
from d3rlpy.metrics.scorer import (AlgoProtocol,
                                   average_value_estimation_scorer,
                                   initial_state_value_estimation_scorer,
                                   td_error_scorer)

from agents.algos import DiscreteAdaptiveCQL


def _d3rlpy_rollout(
	env: gym.Env,
	algo: AlgoProtocol,
	n_trials: int = 20,
	max_horizon: int = 500
	) -> List[Episode]:

	obs_dim = env.observation_space.shape
	
	observations = np.zeros((0, *obs_dim), dtype=np.float32)
	actions = np.zeros(0, dtype=np.int32)
	rewards = np.zeros(0, dtype=np.float32)
	terminals = np.zeros(0, dtype=np.float32)

	for _ in range(n_trials):
		obs = env.reset()
		for _ in range(max_horizon):
			action = algo.predict([obs])[0]
			nxt_obs, reward, done, _ = env.step(action)

			observations = np.append(observations, [obs], axis=0)
			actions = np.append(actions, action)
			rewards = np.append(rewards, reward)
			terminals = np.append(terminals, done)

			obs = nxt_obs
			if done:
				break
		terminals[-1] = 1

	return MDPDataset(
		observations=observations,
		actions=actions,
		rewards=rewards,
		terminals=terminals,
		discrete_action=True
	).episodes

	
def on_policy_initial_state_value_estimation_scorer(
	env: gym.Env, 
	n_trials: int = 10
	) -> Callable[..., float]:
	
	def scorer(algo: AlgoProtocol, *args: Any) -> float:
		# we only need the first state
		eval_episodes = _d3rlpy_rollout(env, algo, n_trials, max_horizon=1)
		return initial_state_value_estimation_scorer(algo, eval_episodes)

	return scorer


def on_policy_average_value_estimation_scorer(
	env: gym.Env, 
	n_trials: int = 10,
	max_horizon: int = 500
	) -> Callable[..., float]:
	
	def scorer(algo: AlgoProtocol, *args: Any) -> float:
		eval_episodes = _d3rlpy_rollout(env, algo, n_trials, max_horizon)
		return average_value_estimation_scorer(algo, eval_episodes)

	return scorer


def on_policy_td_error_scorer(
	env: gym.Env, 
	n_trials: int = 10,
	max_horizon: int = 500
	) -> Callable[..., float]:

	def scorer(algo: AlgoProtocol, *args: Any) -> float:
		eval_episodes = _d3rlpy_rollout(env, algo, n_trials, max_horizon)
		return td_error_scorer(algo, eval_episodes)

	return scorer


def on_policy_decision_percentage(
	env: gym.Env,
	policy_idx: int,
	n_trials: int = 10,
	max_horizon: int = 500
	) -> Callable[..., float]:

	def scorer(algo: AlgoProtocol, *args: Any) -> float:
		assert isinstance(algo, DiscreteAdaptiveCQL), "This scorer can only be used on Adaptive CQL."
		eval_episodes = _d3rlpy_rollout(env, algo, n_trials, max_horizon)
		eval_obs = np.vstack([episode.observations for episode in eval_episodes])
		adaptive_weights = algo.get_adaptive_weights(eval_obs)
		return adaptive_weights.mean(0)[policy_idx]


	return scorer
