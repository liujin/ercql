import os
import random
import shutil
from multiprocessing import Pool

import d3rlpy
import hydra
import numpy as np
import torch
from omegaconf import OmegaConf, open_dict

import custom_envs


def set_random_seed(seed):
	torch.manual_seed(seed)
	np.random.seed(seed)
	random.seed(seed)
	d3rlpy.seed(seed)


def worker(seed, pipeline_config):
	# needs to call again if in a different process
	register_omegaconf_custom_resolver()

	set_random_seed(seed)
	pipeline = hydra.utils.instantiate(pipeline_config)
	pipeline.process()


def register_omegaconf_custom_resolver():
	# add dynamic variable interpolation resolver

	# enable syntax ${add:value1,value2,value3}
	OmegaConf.register_new_resolver(
		"add", lambda *numbers: sum(numbers), replace=True)

	# enable syntax ${mult:value1,value2,value3}
	OmegaConf.register_new_resolver(
		"mult", lambda *numbers: np.prod(numbers), replace=True)

	# enable syntax ${len:list} or ${len:dict}
	OmegaConf.register_new_resolver(
		"len", lambda container: len(container), replace=True)

	# enable syntax ${as_int:number}
	OmegaConf.register_new_resolver(
		"as_int", lambda x: int(x), replace=True)

	# enable syntax ${as_np:array}
	OmegaConf.register_new_resolver(
		"as_np", lambda x: np.array(x), replace=True)


@hydra.main(config_path='./conf', config_name='config')
def main(cfg):
	cfg = OmegaConf.create(cfg)

	if cfg.general.clean_logging_dir:
		if os.path.isdir(cfg.general.logging_dir):
			try:
				shutil.rmtree(cfg.general.logging_dir)
			except OSError as error:
				print('Directory {} can not be removed: {}'.format(cfg.general.logging_dir, error))
	
	arguments = []

	with open_dict(cfg):
		for seed in cfg.experiment.seed_list:
			for pipeline_name in cfg.pipelines:
				# required to set the experiment name here
				# since YAML does not support direct parent value intepretation		
				if cfg.experiment.experiment_prefix == 'single_stage':
					cfg.pipelines[pipeline_name].agent.experiment_name \
						= f'{pipeline_name}_{seed}'

				elif cfg.experiment.experiment_prefix == 'two_stage':
					cfg.pipelines[pipeline_name].second_stage_agent.experiment_name \
						= f'{pipeline_name}_{seed}'
				
				else:
					raise ValueError('Invalid experiment prefix.')
				
				arguments.append((seed, cfg.pipelines[pipeline_name].copy()))
	
	if torch.cuda.is_available() and cfg.general.use_gpu:
		assert cfg.general.n_workers <= 0, 'CUDA and multiprocessing are not compatible...'

	if cfg.general.n_workers <= 0:
		for arg in arguments:
			worker(*arg)
	else:
		with Pool(processes=cfg.general.n_workers) as pool:
			pool.starmap(worker, arguments)

	# saving config files
	OmegaConf.save(cfg, os.path.join(cfg.general.logging_dir, 'config.yaml'))


if __name__ == '__main__':
	register_omegaconf_custom_resolver()
	main()
