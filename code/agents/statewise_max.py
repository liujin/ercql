import copy
from collections import defaultdict
from typing import Any, Callable, Dict, List, Optional, Tuple, Union

import numpy as np
from core.agent import Agent
from core.data_source import DataSource, MixtureD3RLpyDataSource
from d3rlpy.algos.base import AlgoBase
from d3rlpy.dataset import Episode
from d3rlpy.logger import D3RLPyLogger
from d3rlpy.preprocessing import RewardScaler


class StatewiseMaxD3RLpyAgent(Agent):
	algo_list: List[AlgoBase]
	algo_names: List[str]
	logger: D3RLPyLogger
	algo_training_kwargs: Dict[Any, Any]

	n_epochs: int
	n_steps_per_epoch: int
	save_metrics: bool

	action_dim: int
	scorers: Optional[Dict[str, Callable[[Any, List[Episode]], float]]]

	decision_stats: Dict[str, int]
	decision_target: Optional[str]

	def __init__(self, d3rlpy_algo: AlgoBase, n_experts: int, **kwargs):
		self.algo_list = [copy.deepcopy(d3rlpy_algo) for _ in range(n_experts)]
		
		self._prepare_algo_training_kwargs(**kwargs)

		assert 'n_epochs' in kwargs and 'n_steps' not in kwargs, \
			'Only allow specifying n_epochs and n_steps_per_epoch.'

		self.n_epochs = kwargs.get('n_epochs', 1)
		self.n_steps_per_epoch = kwargs.get('n_steps_per_epoch', 10000)
		self.save_metrics = kwargs.get('save_metrics', False)

		self.decision_stats = defaultdict(int)
		self.decision_target = kwargs.get('decision_target', None)
		self.scorers = kwargs.get('scorers', None)

		self._prepare_logger()
		self._trim_kwargs()


	def _prepare_algo_training_kwargs(self, **kwargs) -> None:
		# add some default params to d3rlpy if missing
		default_kwargs = {
			'with_timestamp': True
		}

		default_kwargs.update(kwargs)
		self.algo_training_kwargs = default_kwargs


	def _prepare_logger(self) -> None:
		""" create a seperate logger here and disable all logging behaviors of d3rlpy algorithms """
		
		# follow strictly from the API
		if self.save_metrics:
			self.logger = D3RLPyLogger(
				experiment_name=self.algo_training_kwargs['experiment_name'],
				save_metrics=self.algo_training_kwargs.get('save_metrics', False),
				root_dir=self.algo_training_kwargs['logdir'],
				verbose=self.algo_training_kwargs.get('verbose', False),
				tensorboard_dir=self.algo_training_kwargs.get('tensorboard_dir', None),
				with_timestamp=self.algo_training_kwargs['with_timestamp']
			)


	def _trim_kwargs(self) -> None:
		# take off or overwrite unnecessary arguments to d3rlpy
		self.algo_training_kwargs['n_epochs'] = 1
		self.algo_training_kwargs['save_metrics'] = False

		trimmed_keys = [
			'scorers',
			'eval_episodes',
			'experiment_name',
			'decision_target'
		]

		for k in trimmed_keys:
			self.algo_training_kwargs.pop(k, None)


	def select_greedy_action(
		self, 
		states: np.ndarray
		) -> np.ndarray:
		# states shape [batch_size, state_dim] -> [batch_size * action_dim, state_dim]

		batch_dim = states.shape[0]

		all_actions = np.tile(np.arange(self.action_dim).reshape(-1, 1), (batch_dim, 1))
		dup_states = np.tile(states, (self.action_dim, 1))

		# shape [batch_dim * action_dim, n_experts]
		values = self._predict_raw_value(dup_states, all_actions, with_std=False)
		assert isinstance(values, np.ndarray)

		# reshape to [batch_dim, action_dim, n_experts]
		values = values.reshape(batch_dim, self.action_dim, -1)
		
		# get statewise maximum [batch_dim, action_dim]
		statewise_max_values = np.amax(values, axis=-1)

		# get actions
		actions = np.argmax(statewise_max_values, axis=-1)

		# calculate which algorithm decides the value of an action
		all_action_decisions = np.argmax(values, axis=-1)

		# calculate which algorithm decides the final action of a sample
		max_value_action_decisions = [
			all_action_decisions[i, actions[i]] for i in range(len(actions))]

		# record these decisions
		for decision in max_value_action_decisions:
			self.decision_stats[self.algo_names[decision]] += 1

		return actions


	def train(
		self,
		training_data: DataSource
		) -> None:

		assert isinstance(training_data, MixtureD3RLpyDataSource), \
			'StatewiseMaxDiscreteCQL only accepts MixtureD3RLpyDataSource'

		transition_dict = training_data.get_transitions()
		assert isinstance(transition_dict, dict)
		self.action_dim = next(iter(transition_dict.values()))[0].get_action_size()
		self.algo_names = list(transition_dict.keys())

		for epoch in range(1, self.n_epochs + 1):
			# reset decision stats every epoch
			self.decision_stats.clear()

			# train each algo on one dataset only
			for algo, transition_key in zip(self.algo_list, transition_dict):
				algo.fit(transition_dict[transition_key], **self.algo_training_kwargs)

			# evaluate with scorers
			if self.scorers:
				for name, scorer in self.scorers.items():
					# use [None] here for evaluation episodes, later change to take from arguments
					test_score = scorer(self, [])
					self.logger.add_metric(name, test_score)

				# write decision percentage of a target algorithm
				if self.decision_target is not None:
					total_decision_cnt = sum(list(self.decision_stats.values()))
					if total_decision_cnt > 0:
						target_decision_cnt = self.decision_stats[self.decision_target]
						self.logger.add_metric('decision_perc',
							1. * target_decision_cnt / total_decision_cnt)

				self.logger.commit(epoch, epoch * self.n_steps_per_epoch)


	def predict(
		self, 
		states: Union[np.ndarray, List[Any]]
		) -> np.ndarray:
		""" Alias to select_greedy_action """
		if isinstance(states, list):
			states = np.array(states)
		return self.select_greedy_action(states)


	def predict_value(
		self, 
		states: np.ndarray, 
		actions: np.ndarray,
		with_std: bool = False
		) -> Union[np.ndarray, Tuple[np.ndarray, np.ndarray]]:
		""" right now we just return ensemble-wise maximum for  """
		if with_std:
			raise NotImplementedError
		return np.amax(
			self._predict_raw_value(
				states=states,
				actions=actions,
				with_std=False), 
			axis=-1)


	def _predict_raw_value(
		self, 
		states: np.ndarray, 
		actions: np.ndarray,
		with_std: bool = False
		) -> Union[np.ndarray, Tuple[np.ndarray, np.ndarray]]:
		""" return the results from each algo independently, packed """

		# shape [n_experts, batch_size]
		ret = [algo.predict_value(states, actions, with_std) for algo in self.algo_list]
		
		# return shape [batch_size, n_experts]
		if with_std:
			values, stds = zip(*ret)
			return np.vstack(values).transpose((1, 0)), np.vstack(stds).transpose((1, 0))
		else:
			return np.vstack(ret).transpose((1, 0))


	""" required for d3rlpy.metrics.scorer.AlgoProtocal """
	@property
	def n_frames(self) -> int:
		return 0


	@property
	def gamma(self) -> float:
		# TODO: might need to change later
		return 0.99


	@property
	def reward_scaler(self) -> Optional[RewardScaler]:
		return None
