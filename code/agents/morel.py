from copy import deepcopy
from typing import Any, Callable, Dict, Optional, Tuple, Union

import gym  # type: ignore
import numpy as np
import scipy.spatial  # type: ignore
import torch
import torch.nn as nn
from core.agent import Agent
from core.data_source import D3RLpyDataSource, DataSource
from d3rlpy.dataset import Transition
from d3rlpy.logger import D3RLPyLogger
from stable_baselines3 import PPO
from stable_baselines3.common.evaluation import evaluate_policy
from torch.utils.data import DataLoader, Dataset


class MorelD3rlpyDataset(Dataset):
	def __init__(self, d3rlpy_datasource: D3RLpyDataSource):
		transitions = d3rlpy_datasource.get_transitions()

		assert isinstance(transitions[0], Transition)
		self.action_size = transitions[0].get_action_size()
		self.obs_size = sum(transitions[0].get_observation_shape())

		# input data
		self.obs = np.vstack([t.observation for t in transitions])
		self.actions = np.vstack([t.action for t in transitions])

		# output data
		nxt_obs = np.vstack([t.next_observation for t in transitions])
		self.obs_delta = nxt_obs - self.obs
		self.rewards = np.vstack([t.reward for t in transitions])

		def std_guard(std: np.ndarray) -> np.ndarray:
			std[std == 0] = 1
			return std

		# normalization
		self.data_stats = dict(
			obs_mean=self.obs.mean(axis=0),
			obs_std=std_guard(self.obs.std(axis=0)),

			obs_delta_mean=self.obs_delta.mean(axis=0),
			obs_delta_std=std_guard(self.obs_delta.std(axis=0)),

			reward_mean=self.rewards.mean(axis=0),
			reward_std=std_guard(self.rewards.std(axis=0))
		)

		self.obs = (self.obs - self.data_stats['obs_mean']) / self.data_stats['obs_std']
		self.obs_delta = (self.obs_delta - self.data_stats['obs_delta_mean']) / self.data_stats['obs_delta_std']
		self.rewards = (self.rewards - self.data_stats['reward_mean']) / self.data_stats['reward_std']

		# take out terminals
		terminals = np.vstack([t.terminal for t in transitions]).reshape(-1).astype(bool)

		self.obs = np.delete(self.obs, terminals, axis=0)
		self.actions = np.delete(self.actions, terminals, axis=0)
		self.obs_delta = np.delete(self.obs_delta, terminals, axis=0)
		self.rewards = np.delete(self.rewards, terminals, axis=0)


	def __len__(self):
		return len(self.obs)


	def _one_hot_action(self, idx) -> np.ndarray:
		x = np.zeros(self.action_size)
		x[self.actions[idx]] = 1
		return x


	def __getitem__(self, idx):
		input_tensor = torch.FloatTensor(np.concatenate([self.obs[idx], self._one_hot_action(idx)]))
		output_tensor = torch.FloatTensor(np.concatenate([self.obs_delta[idx], self.rewards[idx]]))
		return input_tensor, output_tensor


class DynamicsNet(nn.Module):
	def __init__(self,
		input_dim: int,
		output_dim: int,
		hidden_dim: int = 64):

		super(DynamicsNet, self).__init__()

		self.layers = nn.Sequential(
			nn.Linear(input_dim, hidden_dim),
			nn.ReLU(),
			nn.Linear(hidden_dim, hidden_dim),
			nn.ReLU(),
			nn.Linear(hidden_dim, hidden_dim),
			nn.ReLU(),
			nn.Linear(hidden_dim, output_dim)
		)

	def forward(self, x) -> torch.Tensor:
		return self.layers(x)


class DynamicsEnsemble:
	def __init__(self,
		obs_size: int,
		action_size: int,
		hidden_dim: int = 64,
		n_models: int = 4,
		threshold: float = 1.0
		):

		self.action_size = action_size
		self.obs_size = obs_size
		self.n_models = n_models
		self.threshold = threshold
		self.models = []
		self.optimizers = []
		self.losses = []

		for _ in range(n_models):
			self.models.append(DynamicsNet(obs_size+action_size, obs_size+1, hidden_dim))
			self.optimizers.append(torch.optim.Adam(self.models[-1].parameters()))
			self.losses.append(nn.MSELoss())


	def _forward(self, model_idx: int, x: torch.Tensor) -> torch.Tensor:
		return self.models[model_idx](x)


	def predict(self, obs: torch.Tensor, action: int) -> torch.Tensor:
		with torch.no_grad():
			one_hot_action = torch.zeros(self.action_size)
			one_hot_action[action] = 1
			x = torch.cat([obs, one_hot_action])
			return torch.stack(list(map(lambda i: self._forward(i, x), range(self.n_models))))


	def usad(self, pred: np.ndarray) -> bool:
		distances = scipy.spatial.distance_matrix(pred, pred)
		return (np.amax(distances) > self.threshold)


	def _train_step(self, model_idx: int, x: torch.Tensor, y: torch.Tensor) -> torch.Tensor:
		self.optimizers[model_idx].zero_grad()

		pred = self.models[model_idx](x)
		loss = self.losses[model_idx](pred, y)

		loss.backward()
		self.optimizers[model_idx].step()

		return loss


	def train(self, 
		dataloader: DataLoader,
		n_epochs: int = 5) -> None:

		n_iter_per_epoch = len(dataloader)
		
		for epoch in range(n_epochs):
			epoch_loss = 0.0
			for i, batch in enumerate(dataloader):
				(x, y) = batch
				loss = list(map(lambda i: self._train_step(i, x, y), range(self.n_models)))
				epoch_loss += float(sum(loss) / self.n_models)

			# print(f'Epoch {epoch}: loss = {epoch_loss / n_iter_per_epoch}')


class MorelEnv(gym.Env):
	def __init__(self,
		real_env: gym.Env,
		dynamics: DynamicsEnsemble,
		start_states: Union[np.ndarray, torch.Tensor],
		data_stats: Dict[str, float],
		uncertainty_penalty: float = -5,
		timeout_steps: int = 500
		):
		super(MorelEnv, self).__init__()
		
		self.dynamics = dynamics
		if not isinstance(start_states, torch.Tensor):
			self.start_states = torch.FloatTensor(start_states)
		self.data_stats = data_stats
		self.uncertainty_penalty = uncertainty_penalty
		
		self.timeout_steps = timeout_steps
		self.steps_elapsed = 0

		self.state: Optional[torch.Tensor] = None
		self.sampler: Optional[Callable[[], np.ndarray]] = None

		# required properties
		self.action_space = deepcopy(real_env.action_space)
		self.observation_space = deepcopy(real_env.observation_space)


	def add_init_state_sampler(self, sampler: Callable[[], np.ndarray]) -> None:
		self.sampler = sampler


	def reset(self) -> np.ndarray:
		self.steps_elapsed = 0
		if self.sampler is not None:
			state_unnormalized = self.sampler()
			self.state = torch.FloatTensor((state_unnormalized - self.data_stats['obs_mean']) / self.data_stats['obs_std'])
		else:
			initial_state_idx = np.random.choice(self.start_states.shape[0])
			self.state = self.start_states[initial_state_idx]
			state_unnormalized = self.state.numpy() * self.data_stats['obs_std'] + self.data_stats['obs_mean']
		return state_unnormalized


	def step(self, action: int) -> Tuple[np.ndarray, float, bool, Dict[Any, Any]]:
		assert self.state is not None, 'Need to call reset before calling step.'

		self.steps_elapsed += 1

		pred = self.dynamics.predict(self.state, action)
		obs_delta, rewards = pred[:, :-1], pred[:, -1:]
		uncertain = self.dynamics.usad(pred.numpy())

		obs_delta_unnormalized = torch.mean(obs_delta, 0) * self.data_stats['obs_delta_std'] + self.data_stats['obs_delta_mean']
		reward_unnormalized = (torch.mean(rewards) * self.data_stats['reward_std'] + self.data_stats['reward_mean']).item()

		state_unnormalized = self.state * self.data_stats['obs_std'] + self.data_stats['obs_mean']
		nxt_state_unnormalized = state_unnormalized + obs_delta_unnormalized

		self.state = (nxt_state_unnormalized - self.data_stats['obs_mean']) / self.data_stats['obs_std']

		if uncertain:
			reward_unnormalized = self.uncertainty_penalty

		return nxt_state_unnormalized.numpy(), reward_unnormalized, (uncertain or self.steps_elapsed >= self.timeout_steps), {}


	def render(self, mode: str = 'human'):
		raise NotImplementedError


	def close(self):
		pass


class MorelAgent(Agent):
	def __init__(self, eval_env: gym.Env, **kwargs):
		self.eval_env = eval_env

		self.dynamics: Optional[DynamicsEnsemble] = None
		self.ppo: Optional[PPO] = None

		self.algo_training_kwargs = kwargs
		self._prepare_logger()


	def _prepare_logger(self) -> None:
		# follow strictly from the API
		if self.algo_training_kwargs.get('save_metrics', False):
			self.logger = D3RLPyLogger(
				experiment_name=self.algo_training_kwargs['experiment_name'],
				save_metrics=self.algo_training_kwargs.get('save_metrics', False),
				root_dir=self.algo_training_kwargs['logdir'],
				verbose=self.algo_training_kwargs.get('verbose', False),
				tensorboard_dir=self.algo_training_kwargs.get('tensorboard_dir', None),
				with_timestamp=self.algo_training_kwargs['with_timestamp']
			)


	def train(self, d3rlpy_datasource: DataSource) -> None:

		assert isinstance(d3rlpy_datasource, D3RLpyDataSource)

		dataset = MorelD3rlpyDataset(d3rlpy_datasource)
		dataloader = DataLoader(
			dataset, 
			batch_size=self.algo_training_kwargs.get('dynamics_batch_size', 64), 
			shuffle=True
		)

		if self.dynamics is None:
			self.dynamics = DynamicsEnsemble(
				obs_size=dataset.obs_size,
				action_size=dataset.action_size,
				hidden_dim=self.algo_training_kwargs.get('dynamics_hidden_dim', 64),
				threshold=self.algo_training_kwargs.get('dynamics_threshold', 1.0)
			)

		self.dynamics.train(
			dataloader, 
			self.algo_training_kwargs.get('dynamics_training_epochs', 500))

		learned_env = MorelEnv(
			real_env=self.eval_env,
			dynamics=self.dynamics,
			start_states=dataset.obs,
			data_stats=dataset.data_stats,
			uncertainty_penalty=self.algo_training_kwargs.get('uncertainty_penalty', -5),
			timeout_steps=self.algo_training_kwargs.get('timeout_steps', 500)
		)

		if self.algo_training_kwargs.get('save_imagined_metrics', False) and self.logger:		
			eval_env_copy = deepcopy(self.eval_env)
			learned_env_with_real_init_distribution = deepcopy(learned_env)
			learned_env_with_real_init_distribution.add_init_state_sampler(
				lambda : eval_env_copy.reset()
			)

		if self.ppo is None:
			self.ppo = PPO('MlpPolicy', learned_env, verbose=self.algo_training_kwargs.get('rl_verbose', 1))

		for epoch in range(self.algo_training_kwargs.get('rl_training_epochs', 100)):
			rl_training_steps = self.algo_training_kwargs.get('rl_training_steps', 5000)
			self.ppo = self.ppo.learn(rl_training_steps)

			saved_anything = False
			if self.algo_training_kwargs.get('save_metrics', False) and self.logger:
				mean, _ = self._evalute_with_env()
				self.logger.add_metric('rollout_return', mean)
				saved_anything = True

			if self.algo_training_kwargs.get('save_imagined_metrics', False) and self.logger:
				mean, _ = self._evalute_with_env(learned_env_with_real_init_distribution)
				self.logger.add_metric('imagined_rollout_return', mean)
				saved_anything = True
			
			if saved_anything:
				self.logger.commit(epoch, epoch * rl_training_steps)


	def _evalute_with_env(self, env: Optional[gym.Env] = None) -> Tuple[float, float]:
		assert self.ppo, 'PPO is not instantiated before calling evaluation.'
		mean, std = evaluate_policy(
			self.ppo, 
			env or self.eval_env, 
			n_eval_episodes=self.algo_training_kwargs.get('rl_n_eval_episodes', 20), 
			deterministic=True,
			warn=False
		)
		assert isinstance(mean, float) and isinstance(std, float)
		return mean, std


	def select_greedy_actions(
		self, 
		states: np.ndarray
		) -> np.ndarray:		
		
		pass

	def predict_values(
		self, 
		states: np.ndarray, 
		actions: np.ndarray,
		with_std: bool = False
		) -> Union[np.ndarray, Tuple[np.ndarray, np.ndarray]]:
		
		pass


