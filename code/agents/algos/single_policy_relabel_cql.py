from typing import Any, Optional, Sequence, Tuple

import torch
import torch.nn.functional as F
from d3rlpy.argument_utility import (EncoderArg, QFuncArg, RewardScalerArg,
                                     ScalerArg, UseGPUArg)
from d3rlpy.gpu import Device
from d3rlpy.logger import LOG
from d3rlpy.models.encoders import EncoderFactory
from d3rlpy.models.optimizers import AdamFactory, OptimizerFactory
from d3rlpy.models.q_functions import QFunctionFactory
from d3rlpy.preprocessing import RewardScaler, Scaler
from policy_wrappers.wrappers import LoggingPolicy, load_logging_policy

from .single_policy_cql import DiscreteSPCQL, DiscreteSPCQLImpl


class DiscreteSPRelabelCQLImpl(DiscreteSPCQLImpl):
	""" This class takes in observation with shape (env_obs_dim + 1), padded by zero """

	_relabeling_policy: LoggingPolicy

	def __init__(
		self,
		observation_shape: Sequence[int],
		action_size: int,
		learning_rate: float,
		optim_factory: OptimizerFactory,
		encoder_factory: EncoderFactory,
		q_func_factory: QFunctionFactory,
		gamma: float,
		n_critics: int,
		alpha: float,
		beta: float,
		use_gpu: Optional[Device],
		scaler: Optional[Scaler],
		reward_scaler: Optional[RewardScaler],
		source_label: int,
		source_label_ratio: float,
		relabeling_policy: LoggingPolicy
	):
		super().__init__(
			observation_shape=observation_shape,
			action_size=action_size,
			learning_rate=learning_rate,
			optim_factory=optim_factory,
			encoder_factory=encoder_factory,
			q_func_factory=q_func_factory,
			gamma=gamma,
			n_critics=n_critics,
			alpha=alpha,
			beta=beta,
			use_gpu=use_gpu,
			scaler=scaler,
			reward_scaler=reward_scaler,
			source_label=source_label,
			source_label_ratio=source_label_ratio
		)

		self._relabeling_policy = relabeling_policy


	def _compute_relabel_actions(self, obs: torch.Tensor) -> torch.Tensor:
		"""
			given a batch of observation, return the greedy actions based on relabeling policy
			TODO: might need to make it gpu compatible
		"""
		# get the observations without padded source
		real_obs = obs[:, :-1].numpy()
		return self._relabeling_policy.predict(real_obs).long()


	def _compute_conservative_loss(
		self, obs_t: torch.Tensor, source_t: torch.Tensor, act_t: torch.Tensor
	) -> Tuple[torch.Tensor, torch.Tensor]:
		assert self._q_func is not None
		# compute logsumexp
		policy_values = self._q_func(obs_t)
		logsumexp = torch.logsumexp(policy_values, dim=1, keepdim=True)

		# calculate relabeled actions for all states in a batch
		relabeled_actions = self._compute_relabel_actions(obs_t)

		# mask for differentiating in and out of source samples
		in_mask = (source_t == self._source_label).float().detach()
		out_mask = (source_t != self._source_label).float().detach()

		real_one_hot = F.one_hot(act_t.view(-1), num_classes=self.action_size)
		fake_one_hot = F.one_hot(relabeled_actions.view(-1), num_classes=self.action_size)

		# estimate action-values
		real_data_values = (policy_values * real_one_hot).sum(dim=1, keepdim=True)
		fake_data_values = (policy_values * fake_one_hot).sum(dim=1, keepdim=True)
		
		# combine them together
		data_values = real_data_values * in_mask + fake_data_values * out_mask

		# """ DEBUG START """
		# with torch.no_grad():
		# 	obs = torch.FloatTensor([[0, 0], [1, 0]])
		# 	# obs = torch.FloatTensor([[1, 0]])
		# 	q = self._q_func(obs)
		# 	print('q: ', q.numpy())
		# """ DEBUG END """

		return torch.mean(logsumexp), torch.mean(data_values)
		# return torch.mean(logsumexp) * 0, torch.mean(data_values) * 0


class DiscreteSPRelabelCQL(DiscreteSPCQL):
	_relabeling_policy_path: str
	_relabeling_policy_dir: str

	def __init__(
		self,
		*,
		learning_rate: float = 6.25e-5,
		optim_factory: OptimizerFactory = AdamFactory(),
		encoder_factory: EncoderArg = "default",
		q_func_factory: QFuncArg = "mean",
		batch_size: int = 32,
		n_frames: int = 1,
		n_steps: int = 1,
		gamma: float = 0.99,
		n_critics: int = 1,
		target_update_interval: int = 8000,
		alpha: float = 1.0,
		beta: float = 1.0,
		use_gpu: UseGPUArg = False,
		scaler: ScalerArg = None,
		reward_scaler: RewardScalerArg = None,
		impl: Optional[DiscreteSPCQLImpl] = None,
		# which policy to regularize over
		source_label: int = 0,
		relabeling_policy_path: str = 'CartPole-v0_replay',
		relabeling_policy_dir: str = './d3rlpy_data/',
		**kwargs: Any,
	):
		super().__init__(
			learning_rate=learning_rate,
			optim_factory=optim_factory,
			encoder_factory=encoder_factory,
			q_func_factory=q_func_factory,
			batch_size=batch_size,
			n_frames=n_frames,
			n_steps=n_steps,
			gamma=gamma,
			n_critics=n_critics,
			target_update_interval=target_update_interval,
			alpha=alpha,
			beta=beta,
			use_gpu=use_gpu,
			scaler=scaler,
			reward_scaler=reward_scaler,
			impl=impl,
			source_label=source_label,
			**kwargs,
		)

		self._relabeling_policy_path = relabeling_policy_path
		self._relabeling_policy_dir = relabeling_policy_dir
		

	def _build_relabeling_policy(self) -> LoggingPolicy:
		return load_logging_policy(
			policy_name=self._relabeling_policy_path, 
			saving_dir=self._relabeling_policy_dir)


	def _create_impl(
		self, observation_shape: Sequence[int], action_size: int
	) -> None:
		self._impl = DiscreteSPRelabelCQLImpl(
			observation_shape=observation_shape,
			# this is a hacky fix for d3rlpy framework
			action_size=max(2, action_size),
			learning_rate=self._learning_rate,
			optim_factory=self._optim_factory,
			encoder_factory=self._encoder_factory,
			q_func_factory=self._q_func_factory,
			gamma=self._gamma,
			n_critics=self._n_critics,
			alpha=self._alpha,
			beta=self._beta,
			use_gpu=self._use_gpu,
			scaler=self._scaler,
			reward_scaler=self._reward_scaler,
			source_label=self._source_label,
			source_label_ratio=self._source_label_ratio,
			relabeling_policy=self._build_relabeling_policy()
		)
		self._impl.build()
