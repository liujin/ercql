from typing import Any, List, Optional, Sequence, Tuple, Union

import numpy as np
import torch
import torch.nn.functional as F
from core.data_source import DataSource, SourcedMixtureD3RLpyDataSource
from d3rlpy.algos.dqn import DoubleDQN
from d3rlpy.algos.torch.dqn_impl import DoubleDQNImpl
from d3rlpy.argument_utility import (EncoderArg, QFuncArg, RewardScalerArg,
                                     ScalerArg, UseGPUArg)
from d3rlpy.gpu import Device
from d3rlpy.models.encoders import EncoderFactory
from d3rlpy.models.optimizers import AdamFactory, OptimizerFactory
from d3rlpy.models.q_functions import QFunctionFactory
from d3rlpy.preprocessing import RewardScaler, Scaler
from d3rlpy.torch_utility import TorchMiniBatch

from .utils import pad_obs, unpack_obs


class DiscreteSPCQLImpl(DoubleDQNImpl):
	""" This class takes in observation with shape (env_obs_dim + 1), padded by zero """

	_alpha: float
	_beta: float
	_source_label: int
	_source_label_ratio: float

	def __init__(
		self,
		observation_shape: Sequence[int],
		action_size: int,
		learning_rate: float,
		optim_factory: OptimizerFactory,
		encoder_factory: EncoderFactory,
		q_func_factory: QFunctionFactory,
		gamma: float,
		n_critics: int,
		alpha: float,
		beta: float,
		use_gpu: Optional[Device],
		scaler: Optional[Scaler],
		reward_scaler: Optional[RewardScaler],
		source_label: int,
		source_label_ratio: float
	):
		super().__init__(
			observation_shape=observation_shape,
			action_size=action_size,
			learning_rate=learning_rate,
			optim_factory=optim_factory,
			encoder_factory=encoder_factory,
			q_func_factory=q_func_factory,
			gamma=gamma,
			n_critics=n_critics,
			use_gpu=use_gpu,
			scaler=scaler,
			reward_scaler=reward_scaler,
		)
		self._alpha = alpha
		self._beta = beta
		self._source_label = source_label
		self._source_label_ratio = source_label_ratio


	def compute_loss(
		self,
		batch: TorchMiniBatch,
		q_tpn: torch.Tensor,
	) -> torch.Tensor:
		assert self._q_func is not None

		obs_zero_padded, obs_source_label = unpack_obs(batch.observations)

		loss = self._q_func.compute_error(
			observations=obs_zero_padded,
			actions=batch.actions.long(),
			rewards=batch.rewards,
			target=q_tpn,
			terminals=batch.terminals,
			gamma=self._gamma**batch.n_steps,
		)

		logsumexp_loss, action_values_loss = self._compute_conservative_loss(
			obs_zero_padded, obs_source_label, batch.actions.long()
		)

		return loss + self._alpha * logsumexp_loss - self._beta * action_values_loss


	def compute_target(self, batch: TorchMiniBatch) -> torch.Tensor:
		assert self._targ_q_func is not None

		with torch.no_grad():
			# # we need to zero out the padding of next_observations
			next_obs_zero_padded, _ = unpack_obs(batch.next_observations)
			next_actions = self._targ_q_func(next_obs_zero_padded)
			max_action = next_actions.argmax(dim=1)
			return self._targ_q_func.compute_target(
				next_obs_zero_padded,
				max_action,
				reduction="min",
			)

	def _compute_conservative_loss(
		self, obs_t: torch.Tensor, source_t: torch.Tensor, act_t: torch.Tensor
	) -> Tuple[torch.Tensor, torch.Tensor]:
		assert self._q_func is not None
		# compute logsumexp
		policy_values = self._q_func(obs_t)
		logsumexp = torch.logsumexp(policy_values, dim=1, keepdim=True)

		# estimate action-values under source_label data distribution
		one_hot = F.one_hot(act_t.view(-1), num_classes=self.action_size)
		data_values = (self._q_func(obs_t) * one_hot).sum(dim=1, keepdim=True)
		
		mask = (source_t == self._source_label).float().detach()

		# avoid having zero elements after the mask
		mask_sum = torch.clamp(torch.sum(mask), min=1.0)

		return torch.sum(logsumexp * mask) / mask_sum, torch.sum(data_values * mask) / mask_sum


	@property
	def source_label_ratio(self) -> float:
		return self._source_label_ratio


	@source_label_ratio.setter
	def source_label_ratio(self, value) -> None:
		self._source_label_ratio = value


class DiscreteSPCQL(DoubleDQN):
	""" Copied and modified based on d3rlpy.algos.DiscreteCQL """

	_alpha: float
	_beta: float
	_impl: Optional[DiscreteSPCQLImpl]
	_source_label: int
	_source_label_ratio: float

	def __init__(
		self,
		*,
		learning_rate: float = 6.25e-5,
		optim_factory: OptimizerFactory = AdamFactory(),
		encoder_factory: EncoderArg = "default",
		q_func_factory: QFuncArg = "mean",
		batch_size: int = 32,
		n_frames: int = 1,
		n_steps: int = 1,
		gamma: float = 0.99,
		n_critics: int = 1,
		target_update_interval: int = 8000,
		alpha: float = 1.0,
		beta: float = 1.0,
		use_gpu: UseGPUArg = False,
		scaler: ScalerArg = None,
		reward_scaler: RewardScalerArg = None,
		impl: Optional[DiscreteSPCQLImpl] = None,
		# which policy to regularize over
		source_label: int = 0,
		**kwargs: Any,
	):
		super().__init__(
			learning_rate=learning_rate,
			optim_factory=optim_factory,
			encoder_factory=encoder_factory,
			q_func_factory=q_func_factory,
			batch_size=batch_size,
			n_frames=n_frames,
			n_steps=n_steps,
			gamma=gamma,
			n_critics=n_critics,
			target_update_interval=target_update_interval,
			use_gpu=use_gpu,
			scaler=scaler,
			reward_scaler=reward_scaler,
			impl=impl,
			**kwargs,
		)
		self._alpha = alpha
		self._beta = beta
		self._source_label = source_label
		self._source_label_ratio = 1

	def _create_impl(
		self, observation_shape: Sequence[int], action_size: int
	) -> None:
		self._impl = DiscreteSPCQLImpl(
			observation_shape=observation_shape,
			action_size=action_size,
			learning_rate=self._learning_rate,
			optim_factory=self._optim_factory,
			encoder_factory=self._encoder_factory,
			q_func_factory=self._q_func_factory,
			gamma=self._gamma,
			n_critics=self._n_critics,
			alpha=self._alpha,
			beta=self._beta,
			use_gpu=self._use_gpu,
			scaler=self._scaler,
			reward_scaler=self._reward_scaler,
			source_label=self._source_label,
			source_label_ratio=self._source_label_ratio
		)
		self._impl.build()


	def predict(self, x: Union[np.ndarray, List[Any]]) -> np.ndarray:
		return super().predict(pad_obs(x))


	def predict_value(
		self,
		x: Union[np.ndarray, List[Any]],
		action: Union[np.ndarray, List[Any]],
		with_std: bool = False,
	) -> Union[np.ndarray, Tuple[np.ndarray, np.ndarray]]:
		return super().predict_value(pad_obs(x), action, with_std)


	def sample_action(self, x: Union[np.ndarray, List[Any]]) -> np.ndarray:
		return super().sample_action(pad_obs(x))


	def set_data_source(self, data_source: Optional[DataSource]) -> None:
		assert isinstance(data_source, SourcedMixtureD3RLpyDataSource)

		label_ratio = data_source.get_label_ratio(self._source_label)
		self._source_label_ratio = label_ratio
		if self._impl is not None:
			self._impl.source_label_ratio = label_ratio		


	@property
	def source_label(self):
		return self._source_label
