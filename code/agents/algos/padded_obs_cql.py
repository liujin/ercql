import math
from typing import (Any, Dict, List, Optional, Sequence, Tuple, Union,
                    no_type_check)

import numpy as np
import torch
from d3rlpy.algos.base import AlgoBase
from d3rlpy.algos.torch.sac_impl import SACImpl
from d3rlpy.argument_utility import (ActionScalerArg, EncoderArg, QFuncArg,
                                     RewardScalerArg, ScalerArg, UseGPUArg,
                                     check_encoder, check_q_func,
                                     check_use_gpu)
from d3rlpy.constants import IMPL_NOT_INITIALIZED_ERROR, ActionSpace
from d3rlpy.dataset import TransitionMiniBatch
from d3rlpy.gpu import Device
from d3rlpy.models.builders import create_parameter
from d3rlpy.models.encoders import EncoderFactory
from d3rlpy.models.optimizers import AdamFactory, OptimizerFactory
from d3rlpy.models.q_functions import QFunctionFactory
from d3rlpy.models.torch import Parameter
from d3rlpy.preprocessing import ActionScaler, RewardScaler, Scaler
from d3rlpy.torch_utility import TorchMiniBatch, torch_api, train_api
from torch.optim import Optimizer

from .utils import pad_obs, unpack_obs


class PaddedObsCQLImpl(SACImpl):
	_alpha_learning_rate: float
	_alpha_optim_factory: OptimizerFactory
	_initial_alpha: float
	_alpha_threshold: float
	_conservative_weight: float
	_n_action_samples: int
	_soft_q_backup: bool
	_log_alpha: Optional[Parameter]
	_alpha_optim: Optional[Optimizer]


	def __init__(
		self,
		observation_shape: Sequence[int],
		action_size: int,
		actor_learning_rate: float,
		critic_learning_rate: float,
		temp_learning_rate: float,
		alpha_learning_rate: float,
		actor_optim_factory: OptimizerFactory,
		critic_optim_factory: OptimizerFactory,
		temp_optim_factory: OptimizerFactory,
		alpha_optim_factory: OptimizerFactory,
		actor_encoder_factory: EncoderFactory,
		critic_encoder_factory: EncoderFactory,
		q_func_factory: QFunctionFactory,
		gamma: float,
		tau: float,
		n_critics: int,
		initial_temperature: float,
		initial_alpha: float,
		alpha_threshold: float,
		conservative_weight: float,
		n_action_samples: int,
		soft_q_backup: bool,
		use_gpu: Optional[Device],
		scaler: Optional[Scaler],
		action_scaler: Optional[ActionScaler],
		reward_scaler: Optional[RewardScaler],
	):
		super().__init__(
			observation_shape=observation_shape,
			action_size=action_size,
			actor_learning_rate=actor_learning_rate,
			critic_learning_rate=critic_learning_rate,
			temp_learning_rate=temp_learning_rate,
			actor_optim_factory=actor_optim_factory,
			critic_optim_factory=critic_optim_factory,
			temp_optim_factory=temp_optim_factory,
			actor_encoder_factory=actor_encoder_factory,
			critic_encoder_factory=critic_encoder_factory,
			q_func_factory=q_func_factory,
			gamma=gamma,
			tau=tau,
			n_critics=n_critics,
			initial_temperature=initial_temperature,
			use_gpu=use_gpu,
			scaler=scaler,
			action_scaler=action_scaler,
			reward_scaler=reward_scaler,
		)

		self._alpha_learning_rate = alpha_learning_rate
		self._alpha_optim_factory = alpha_optim_factory
		self._initial_alpha = initial_alpha
		self._alpha_threshold = alpha_threshold
		self._conservative_weight = conservative_weight
		self._n_action_samples = n_action_samples
		self._soft_q_backup = soft_q_backup


	def build(self) -> None:
		self._build_alpha()
		super().build()
		self._build_alpha_optim()


	def _build_alpha(self) -> None:
		initial_val = math.log(self._initial_alpha)
		self._log_alpha = create_parameter((1, 1), initial_val)


	def _build_alpha_optim(self) -> None:
		assert self._log_alpha is not None
		self._alpha_optim = self._alpha_optim_factory.create(
			self._log_alpha.parameters(), lr=self._alpha_learning_rate
		)


	@no_type_check # due to the typing error in d3rlpy package
	@train_api
	@torch_api()
	def update_alpha(self, batch: TorchMiniBatch) -> np.ndarray:
		assert self._alpha_optim is not None
		assert self._q_func is not None
		assert self._log_alpha is not None

		obs_zero_padded, source = unpack_obs(batch.observations)
		next_obs_zero_padded, _ = unpack_obs(batch.next_observations)

		# Q function should be inference mode for stability
		self._q_func.eval()

		self._alpha_optim.zero_grad()

		# the original implementation does scale the loss value
		loss = -self._compute_conservative_loss(
			obs_t=obs_zero_padded, 
			src_t=source, 
			act_t=batch.actions,
			obs_tp1=next_obs_zero_padded
		)

		loss.backward()
		self._alpha_optim.step()

		cur_alpha = self._log_alpha().exp().cpu().detach().numpy()[0][0]

		return loss.cpu().detach().numpy(), cur_alpha


	@no_type_check # due to the typing error in d3rlpy package
	@train_api
	@torch_api()
	def update_temp(
		self, batch: TorchMiniBatch
	) -> Tuple[np.ndarray, np.ndarray]:
		assert self._temp_optim is not None
		assert self._policy is not None
		assert self._log_temp is not None

		obs_zero_padded, _ = unpack_obs(batch.observations)

		self._temp_optim.zero_grad()

		with torch.no_grad():
			_, log_prob = self._policy.sample_with_log_prob(obs_zero_padded)
			targ_temp = log_prob - self._action_size

		loss = -(self._log_temp().exp() * targ_temp).mean()

		loss.backward()
		self._temp_optim.step()

		# current temperature value
		cur_temp = self._log_temp().exp().cpu().detach().numpy()[0][0]

		return loss.cpu().detach().numpy(), cur_temp


	def _compute_policy_is_values(
		self, policy_obs: torch.Tensor, value_obs: torch.Tensor
	) -> torch.Tensor:
		assert self._policy is not None
		assert self._q_func is not None
		with torch.no_grad():
			policy_actions, n_log_probs = self._policy.sample_n_with_log_prob(
				policy_obs, self._n_action_samples
			)

		obs_shape = value_obs.shape

		repeated_obs = value_obs.expand(self._n_action_samples, *obs_shape)
		# (n, batch, observation) -> (batch, n, observation)
		transposed_obs = repeated_obs.transpose(0, 1)
		# (batch, n, observation) -> (batch * n, observation)
		flat_obs = transposed_obs.reshape(-1, *obs_shape[1:])
		# (batch, n, action) -> (batch * n, action)
		flat_policy_acts = policy_actions.reshape(-1, self.action_size)

		# estimate action-values for policy actions
		policy_values = self._q_func(flat_obs, flat_policy_acts, "none")
		policy_values = policy_values.view(
			self._n_critics, obs_shape[0], self._n_action_samples
		)
		log_probs = n_log_probs.view(1, -1, self._n_action_samples)

		# importance sampling
		return policy_values - log_probs


	def _compute_random_is_values(self, obs: torch.Tensor) -> torch.Tensor:
		assert self._q_func is not None

		repeated_obs = obs.expand(self._n_action_samples, *obs.shape)
		# (n, batch, observation) -> (batch, n, observation)
		transposed_obs = repeated_obs.transpose(0, 1)
		# (batch, n, observation) -> (batch * n, observation)
		flat_obs = transposed_obs.reshape(-1, *obs.shape[1:])

		# estimate action-values for actions from uniform distribution
		# uniform distribution between [-1.0, 1.0]
		flat_shape = (obs.shape[0] * self._n_action_samples, self._action_size)
		zero_tensor = torch.zeros(flat_shape, device=self._device)
		random_actions = zero_tensor.uniform_(-1.0, 1.0)
		random_values = self._q_func(flat_obs, random_actions, "none")
		random_values = random_values.view(
			self._n_critics, obs.shape[0], self._n_action_samples
		)
		random_log_probs = math.log(0.5**self._action_size)

		# importance sampling
		return random_values - random_log_probs


	def _compute_conservative_loss(
		self,
		obs_t: torch.Tensor, 
		src_t: torch.Tensor, 
		act_t: torch.Tensor,
		obs_tp1: torch.Tensor
	) -> torch.Tensor:
		assert self._policy is not None
		assert self._q_func is not None
		assert self._log_alpha is not None

		policy_values_t = self._compute_policy_is_values(obs_t, obs_t)
		policy_values_tp1 = self._compute_policy_is_values(obs_tp1, obs_t)
		random_values = self._compute_random_is_values(obs_t)

		# compute logsumexp
		# (n critics, batch, 3 * n samples) -> (n critics, batch, 1)
		target_values = torch.cat(
			[policy_values_t, policy_values_tp1, random_values], dim=2
		)
		logsumexp = torch.logsumexp(target_values, dim=2, keepdim=True)

		# estimate action-values for data actions
		data_values = self._q_func(obs_t, act_t, "none")

		loss = logsumexp.mean(dim=0).mean() - data_values.mean(dim=0).mean()
		scaled_loss = self._conservative_weight * loss

		# clip for stability
		clipped_alpha = self._log_alpha().exp().clamp(0, 1e6)[0][0]

		return clipped_alpha * (scaled_loss - self._alpha_threshold)


	def compute_target(self, batch: TorchMiniBatch) -> torch.Tensor:
		assert self._policy
		assert self._targ_q_func

		next_obs_zero_padded, _ = unpack_obs(batch.next_observations)

		if self._soft_q_backup:
			assert self._log_temp is not None
			with torch.no_grad():
				action, log_prob = self._policy.sample_with_log_prob(
					next_obs_zero_padded
				)
				entropy = self._log_temp().exp() * log_prob
				target = self._targ_q_func.compute_target(
					next_obs_zero_padded,
					action,
					reduction="min",
				)
				return target - entropy
		else:
			with torch.no_grad():
				action = self._policy.best_action(next_obs_zero_padded)
				return self._targ_q_func.compute_target(
					next_obs_zero_padded,
					action,
					reduction="min",
				)


	def compute_critic_loss(
		self, batch: TorchMiniBatch, q_tpn: torch.Tensor
	) -> torch.Tensor:
		assert self._q_func is not None

		obs_zero_padded, source = unpack_obs(batch.observations)
		next_obs_zero_padded, _ = unpack_obs(batch.next_observations)

		loss = self._q_func.compute_error(
			observations=obs_zero_padded,
			actions=batch.actions,
			rewards=batch.rewards,
			target=q_tpn,
			terminals=batch.terminals,
			gamma=self._gamma**batch.n_steps,
		)

		conservative_loss = self._compute_conservative_loss(
			obs_t=obs_zero_padded, 
			src_t=source, 
			act_t=batch.actions,
			obs_tp1=next_obs_zero_padded
		)
		
		return loss + conservative_loss


	def compute_actor_loss(self, batch: TorchMiniBatch) -> torch.Tensor:
		assert self._policy is not None
		assert self._log_temp is not None
		assert self._q_func is not None

		obs_zero_padded, _ = unpack_obs(batch.observations)

		action, log_prob = self._policy.sample_with_log_prob(obs_zero_padded)
		entropy = self._log_temp().exp() * log_prob
		q_t = self._q_func(obs_zero_padded, action, "min")
		return (entropy - q_t).mean()


class PaddedObsCQL(AlgoBase):
	_actor_learning_rate: float
	_critic_learning_rate: float
	_temp_learning_rate: float
	_alpha_learning_rate: float
	_actor_optim_factory: OptimizerFactory
	_critic_optim_factory: OptimizerFactory
	_temp_optim_factory: OptimizerFactory
	_alpha_optim_factory: OptimizerFactory
	_actor_encoder_factory: EncoderFactory
	_critic_encoder_factory: EncoderFactory
	_q_func_factory: QFunctionFactory
	_tau: float
	_n_critics: int
	_initial_temperature: float
	_initial_alpha: float
	_alpha_threshold: float
	_conservative_weight: float
	_n_action_samples: int
	_soft_q_backup: bool
	_use_gpu: Optional[Device]
	_impl: Optional[PaddedObsCQLImpl]


	def __init__(
		self,
		*,
		actor_learning_rate: float = 1e-4,
		critic_learning_rate: float = 3e-4,
		temp_learning_rate: float = 1e-4,
		alpha_learning_rate: float = 1e-4,
		actor_optim_factory: OptimizerFactory = AdamFactory(),
		critic_optim_factory: OptimizerFactory = AdamFactory(),
		temp_optim_factory: OptimizerFactory = AdamFactory(),
		alpha_optim_factory: OptimizerFactory = AdamFactory(),
		actor_encoder_factory: EncoderArg = "default",
		critic_encoder_factory: EncoderArg = "default",
		q_func_factory: QFuncArg = "mean",
		batch_size: int = 256,
		n_frames: int = 1,
		n_steps: int = 1,
		gamma: float = 0.99,
		tau: float = 0.005,
		n_critics: int = 2,
		initial_temperature: float = 1.0,
		initial_alpha: float = 1.0,
		alpha_threshold: float = 10.0,
		conservative_weight: float = 5.0,
		n_action_samples: int = 10,
		soft_q_backup: bool = False,
		use_gpu: UseGPUArg = False,
		scaler: ScalerArg = None,
		action_scaler: ActionScalerArg = None,
		reward_scaler: RewardScalerArg = None,
		impl: Optional[PaddedObsCQLImpl] = None,
		**kwargs: Any,
	):
		super().__init__(
			batch_size=batch_size,
			n_frames=n_frames,
			n_steps=n_steps,
			gamma=gamma,
			scaler=scaler,
			action_scaler=action_scaler,
			reward_scaler=reward_scaler,
			**kwargs
		)

		self._actor_learning_rate = actor_learning_rate
		self._critic_learning_rate = critic_learning_rate
		self._temp_learning_rate = temp_learning_rate
		self._alpha_learning_rate = alpha_learning_rate
		self._actor_optim_factory = actor_optim_factory
		self._critic_optim_factory = critic_optim_factory
		self._temp_optim_factory = temp_optim_factory
		self._alpha_optim_factory = alpha_optim_factory
		self._actor_encoder_factory = check_encoder(actor_encoder_factory)
		self._critic_encoder_factory = check_encoder(critic_encoder_factory)
		self._q_func_factory = check_q_func(q_func_factory)
		self._tau = tau
		self._n_critics = n_critics
		self._initial_temperature = initial_temperature
		self._initial_alpha = initial_alpha
		self._alpha_threshold = alpha_threshold
		self._conservative_weight = conservative_weight
		self._n_action_samples = n_action_samples
		self._soft_q_backup = soft_q_backup
		self._use_gpu = check_use_gpu(use_gpu)
		self._impl = impl


	def _update(self, batch: TransitionMiniBatch) -> Dict[str, float]:
		assert self._impl is not None, IMPL_NOT_INITIALIZED_ERROR

		metrics = {}

		# lagrangian parameter update for SAC temperature
		if self._temp_learning_rate > 0:
			temp_loss, temp = self._impl.update_temp(batch)
			metrics.update({"temp_loss": temp_loss, "temp": temp})

		# lagrangian parameter update for conservative loss weight
		if self._alpha_learning_rate > 0:
			alpha_loss, alpha = self._impl.update_alpha(batch)
			metrics.update({"alpha_loss": alpha_loss, "alpha": alpha})

		critic_loss = self._impl.update_critic(batch)
		metrics.update({"critic_loss": critic_loss})

		actor_loss = self._impl.update_actor(batch)
		metrics.update({"actor_loss": actor_loss})

		self._impl.update_critic_target()
		self._impl.update_actor_target()

		return metrics


	def _create_impl(
		self, observation_shape: Sequence[int], action_size: int
	) -> None:
		self._impl = PaddedObsCQLImpl(
			observation_shape=observation_shape,
			action_size=action_size,
			actor_learning_rate=self._actor_learning_rate,
			critic_learning_rate=self._critic_learning_rate,
			temp_learning_rate=self._temp_learning_rate,
			alpha_learning_rate=self._alpha_learning_rate,
			actor_optim_factory=self._actor_optim_factory,
			critic_optim_factory=self._critic_optim_factory,
			temp_optim_factory=self._temp_optim_factory,
			alpha_optim_factory=self._alpha_optim_factory,
			actor_encoder_factory=self._actor_encoder_factory,
			critic_encoder_factory=self._critic_encoder_factory,
			q_func_factory=self._q_func_factory,
			gamma=self._gamma,
			tau=self._tau,
			n_critics=self._n_critics,
			initial_temperature=self._initial_temperature,
			initial_alpha=self._initial_alpha,
			alpha_threshold=self._alpha_threshold,
			conservative_weight=self._conservative_weight,
			n_action_samples=self._n_action_samples,
			soft_q_backup=self._soft_q_backup,
			use_gpu=self._use_gpu,
			scaler=self._scaler,
			action_scaler=self._action_scaler,
			reward_scaler=self._reward_scaler
		)
		self._impl.build()


	def predict(self, x: Union[np.ndarray, List[Any]]) -> np.ndarray:
		return super().predict(pad_obs(x))


	def predict_value(
		self,
		x: Union[np.ndarray, List[Any]],
		action: Union[np.ndarray, List[Any]],
		with_std: bool = False,
	) -> Union[np.ndarray, Tuple[np.ndarray, np.ndarray]]:
		return super().predict_value(pad_obs(x), action, with_std)


	def sample_action(self, x: Union[np.ndarray, List[Any]]) -> np.ndarray:
		return super().sample_action(pad_obs(x))


	def get_action_type(self) -> ActionSpace:
		return ActionSpace.CONTINUOUS
