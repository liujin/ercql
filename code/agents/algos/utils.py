from typing import Any, List, Tuple, Union

import numpy as np
import torch


def unpack_obs(obs: torch.Tensor) -> Tuple[torch.Tensor, torch.Tensor]:
	""" 
		unpack obs of shape (B, D) into (B, D) and (B, 1)
		where the (B, D) has last column being all 0
		and (B, 1) is the source label of each obs
	"""
	source_label = obs[:, -1].detach().clone().reshape(-1, 1).int()
	obs[:, -1] = 0

	return obs, source_label


def pad_obs(x: Union[np.ndarray, List[Any]]) -> Union[np.ndarray, List[Any]]:
	if isinstance(x, List):
		x = np.array(x)
	assert isinstance(x, np.ndarray), "Currently only support np.ndarray for padding observations."
	return np.c_[x, np.zeros(x.shape[0])]
