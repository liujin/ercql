from typing import (Any, Callable, Dict, List, Optional, Sequence, Tuple,
                    Union, cast, no_type_check)

import gym  # type: ignore
import numpy as np
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
from d3rlpy.algos.dqn import DoubleDQN
from d3rlpy.algos.torch.dqn_impl import DoubleDQNImpl
from d3rlpy.argument_utility import (ActionScalerArg, EncoderArg, QFuncArg,
                                     RewardScalerArg, ScalerArg, UseGPUArg)
from d3rlpy.base import LearnableBase
from d3rlpy.constants import (CONTINUOUS_ACTION_SPACE_MISMATCH_ERROR,
                              DISCRETE_ACTION_SPACE_MISMATCH_ERROR,
                              IMPL_NOT_INITIALIZED_ERROR, ActionSpace)
from d3rlpy.dataset import Episode, MDPDataset, Transition, TransitionMiniBatch
from d3rlpy.gpu import Device
from d3rlpy.iterators import RoundIterator, TransitionIterator
from d3rlpy.logger import LOG
from d3rlpy.models.encoders import EncoderFactory
from d3rlpy.models.optimizers import AdamFactory, OptimizerFactory
from d3rlpy.models.q_functions import QFunctionFactory
from d3rlpy.preprocessing import ActionScaler, RewardScaler, Scaler
from d3rlpy.torch_utility import TorchMiniBatch, torch_api, train_api
from policy_wrappers.wrappers import LoggingPolicy, load_logging_policy
from tqdm import tqdm  # type: ignore

from .padded_obs_cql import PaddedObsCQL, PaddedObsCQLImpl
from .utils import pad_obs, unpack_obs


class DiscreteAdaptiveCQLImpl(DoubleDQNImpl):
	""" 
	This class takes in observation with shape (env_obs_dim + 1), padded by zero 
	WARNING: This adds extra network (nn.Module), which means it needs extra handling for
		model saving and loading. Currently no implementation for this.
	"""

	_alpha: float
	_beta: float
	_logging_policies: List[LoggingPolicy]
	_z_net: Optional[nn.Module]
	_z_optim: Optional[optim.Optimizer]

	def __init__(
		self,
		observation_shape: Sequence[int],
		action_size: int,
		learning_rate: float,
		optim_factory: OptimizerFactory,
		encoder_factory: EncoderFactory,
		q_func_factory: QFunctionFactory,
		gamma: float,
		n_critics: int,
		alpha: float,
		beta: float,
		use_gpu: Optional[Device],
		scaler: Optional[Scaler],
		reward_scaler: Optional[RewardScaler],
		# extra params for adaptive CQL
		logging_policies: List[LoggingPolicy]
	):
		super().__init__(
			observation_shape=observation_shape,
			action_size=action_size,
			learning_rate=learning_rate,
			optim_factory=optim_factory,
			encoder_factory=encoder_factory,
			q_func_factory=q_func_factory,
			gamma=gamma,
			n_critics=n_critics,
			use_gpu=use_gpu,
			scaler=scaler,
			reward_scaler=reward_scaler,
		)
		self._alpha = alpha
		self._beta = beta
		self._logging_policies = logging_policies
		self._z_loss = nn.CrossEntropyLoss()


	def compute_loss(
		self,
		batch: TorchMiniBatch,
		q_tpn: torch.Tensor,
	) -> torch.Tensor:
		assert self._q_func is not None

		obs_zero_padded, obs_source_label = unpack_obs(batch.observations)

		loss = self._q_func.compute_error(
			observations=obs_zero_padded,
			actions=batch.actions.long(),
			rewards=batch.rewards,
			target=q_tpn,
			terminals=batch.terminals,
			gamma=self._gamma**batch.n_steps,
		)

		logsumexp, policy_value_under_z, divergence = self._compute_conservative_loss(
			obs_t=obs_zero_padded,
			src_t=obs_source_label.long(),
			act_t=batch.actions.long())

		return loss + self._alpha * (logsumexp - policy_value_under_z) + self._beta * divergence


	def compute_target(self, batch: TorchMiniBatch) -> torch.Tensor:
		assert self._targ_q_func is not None

		with torch.no_grad():
			# # we need to zero out the padding of next_observations
			next_obs_zero_padded, _ = unpack_obs(batch.next_observations)
			next_actions = self._targ_q_func(next_obs_zero_padded)
			max_action = next_actions.argmax(dim=1)
			return self._targ_q_func.compute_target(
				next_obs_zero_padded,
				max_action,
				reduction="min",
			)


	def _compute_logging_policy_dist(self, obs_t: torch.Tensor) -> torch.Tensor:
		# baseline3 agent needs to take off the padding dim of environment
		obs_no_padding = obs_t[...,:-1]

		# shape (N, B, A)
		distributions: List[torch.Tensor] = []

		with torch.no_grad(): 
			distributions = [oracle.get_action_pmf(obs_no_padding) for oracle in self._logging_policies]
			# return shape (B, A, N)
			return torch.permute(torch.stack(distributions), (1, 2, 0))


	def get_adaptive_weights(
		self, 
		obs_t: torch.Tensor, 
		training: bool = True
	) -> Tuple[torch.Tensor, torch.Tensor]:
		assert self._z_net is not None
		self._z_net.train(training)

		unnormalized_policy_weights = self._z_net(obs_t)
		normalized_policy_weights = F.softmax(unnormalized_policy_weights, dim=-1)

		self._z_net.train(True)
		return unnormalized_policy_weights, normalized_policy_weights


	def _compute_conservative_loss(
		self, 
		obs_t: torch.Tensor, 
		src_t: torch.Tensor, 
		act_t: torch.Tensor
	) -> Tuple[torch.Tensor, torch.Tensor, torch.Tensor]:
		"""
		outputs: average log sum exponential, divergence term, expected policy values under z_net
		"""
		assert self._q_func is not None

		# compute logsumexp
		policy_values = self._q_func(obs_t)
		logsumexp = torch.logsumexp(policy_values, dim=1, keepdim=True)

		# adaptive network that outputs our belief on which policy to stay close to at state s
		unnormalized_policy_weights, normalized_policy_weights = self.get_adaptive_weights(obs_t)

		# compute the regularizer
		divergence = self._z_loss(unnormalized_policy_weights, src_t.squeeze(-1))
		
		# compute adaptive Q value loss [B, A, N] where N = # of logging policies
		logging_policy_dist = self._compute_logging_policy_dist(obs_t)

		# shape (B, A)
		z_dist = (normalized_policy_weights.unsqueeze(1) * logging_policy_dist).sum(dim=-1)

		# expected policy values under z (B)
		policy_value_under_z = (z_dist * policy_values).sum(dim=-1)

		return logsumexp.mean(), policy_value_under_z.mean(), divergence


	@no_type_check # due to the typing error in d3rlpy package
	@train_api
	@torch_api(scaler_targets=["obs_t", "obs_tpn"])
	def update(self, batch: TorchMiniBatch) -> np.ndarray:
		assert self._optim is not None
		assert self._z_optim is not None

		self._optim.zero_grad()
		self._z_optim.zero_grad()

		q_tpn = self.compute_target(batch)
		loss = self.compute_loss(batch, q_tpn)

		loss.backward()
		self._optim.step()
		self._z_optim.step()

		# """ DEBUG START """
		# with torch.no_grad():
		# 	obs = torch.FloatTensor([[0, 0], [1, 0]])
		# 	q = self._q_func(obs)
		# 	_, z = self.get_adaptive_weights(obs)
		# 	print('q: ', q.numpy())
		# 	print('z: ', z.numpy())
		# """ DEBUG END """

		return loss.cpu().detach().numpy()


	@no_type_check # due to the typing error in d3rlpy package
	@train_api
	@torch_api(scaler_targets=["obs_t", "obs_tpn"])
	def update_z_net(self, batch: TorchMiniBatch) -> np.ndarray:
		""" pretraining procedure """
		assert self._z_net is not None
		assert self._z_optim is not None

		self._z_optim.zero_grad()

		# disassemble observations
		obs_zero_padded, obs_source_label = unpack_obs(batch.observations)
		# compute z weights
		unnormalized_policy_weights, _ = self.get_adaptive_weights(obs_zero_padded)
		# compute loss
		loss = self._z_loss(unnormalized_policy_weights, obs_source_label.squeeze(-1).long())

		loss.backward()
		self._z_optim.step()

		return loss.cpu().detach().numpy()


	def _build_network(self) -> None:
		super()._build_network()

		# create a new encoder (later might add option for sharing this with the Q func)
		encoder = self._encoder_factory.create(self._observation_shape)

		# output raw (unnormalized) weights because we will use BCE later
		self._z_net = nn.Sequential(
			cast(nn.Module, encoder),
			nn.Linear(encoder.get_feature_size(), 128),
			nn.ReLU(),
			nn.Linear(128, 128),
			nn.ReLU(),
			nn.Linear(128, len(self._logging_policies)))


	def _build_optim(self) -> None:
		super()._build_optim()

		assert self._z_net is not None
		self._z_optim = optim.Adam(
			self._z_net.parameters(), lr=self._learning_rate)


class DiscreteAdaptiveCQL(DoubleDQN):
	""" Copied and modified based on d3rlpy.algos.DiscreteCQL """

	_alpha: float
	_beta: float
	_impl: Optional[DiscreteAdaptiveCQLImpl]
	_logging_policy_names: List[str]
	_logging_policy_dir: str

	def __init__(
		self,
		*,
		learning_rate: float = 6.25e-5,
		optim_factory: OptimizerFactory = AdamFactory(),
		encoder_factory: EncoderArg = "default",
		q_func_factory: QFuncArg = "mean",
		batch_size: int = 32,
		n_frames: int = 1,
		n_steps: int = 1,
		gamma: float = 0.99,
		n_critics: int = 1,
		target_update_interval: int = 8000,
		alpha: float = 1.0,
		beta: float = 1.0,
		use_gpu: UseGPUArg = False,
		scaler: ScalerArg = None,
		reward_scaler: RewardScalerArg = None,
		# extra arguments for Adaptive CQL
		logging_policy_names: List[str],
		logging_policy_dir: str = './d3rlpy_data',
		impl: Optional[DiscreteAdaptiveCQLImpl] = None,
		**kwargs: Any
	):
		super().__init__(
			learning_rate=learning_rate,
			optim_factory=optim_factory,
			encoder_factory=encoder_factory,
			q_func_factory=q_func_factory,
			batch_size=batch_size,
			n_frames=n_frames,
			n_steps=n_steps,
			gamma=gamma,
			n_critics=n_critics,
			target_update_interval=target_update_interval,
			use_gpu=use_gpu,
			scaler=scaler,
			reward_scaler=reward_scaler,
			impl=impl,
			**kwargs,
		)
		self._alpha = alpha
		self._beta = beta
		self._logging_policy_names = list(logging_policy_names)
		self._logging_policy_dir = logging_policy_dir


	def _build_logging_policies(self) -> List[LoggingPolicy]:
		# use the same device for logging policy
		device = 'cpu'
		if self._use_gpu is not None:
			device = f'cuda:{self._use_gpu.get_id()}'

		return [load_logging_policy(
			policy_name=name, 
			saving_dir=self._logging_policy_dir, 
			device=device) for name in self._logging_policy_names]


	def _create_impl(
		self, observation_shape: Sequence[int], action_size: int
	) -> None:
		self._impl = DiscreteAdaptiveCQLImpl(
			observation_shape=observation_shape,
			# this is a hacky fix for d3rlpy framework
			action_size=max(2, action_size),
			learning_rate=self._learning_rate,
			optim_factory=self._optim_factory,
			encoder_factory=self._encoder_factory,
			q_func_factory=self._q_func_factory,
			gamma=self._gamma,
			n_critics=self._n_critics,
			alpha=self._alpha,
			beta=self._beta,
			use_gpu=self._use_gpu,
			scaler=self._scaler,
			reward_scaler=self._reward_scaler,
			# extra arguments
			logging_policies=self._build_logging_policies()
		)
		self._impl.build()


	def _pretrain_z_net(
		self, 
		dataset: Union[List[Episode], List[Transition], MDPDataset],
		shuffle: bool,
		show_progress: bool,
		n_epochs: int = 50
	) -> None:

		transitions = []
		if isinstance(dataset, MDPDataset):
			for episode in dataset.episodes:
				transitions += episode.transitions
		elif not dataset:
			raise ValueError("empty dataset is not supported.")
		elif isinstance(dataset[0], Episode):
			for episode in cast(List[Episode], dataset):
				transitions += episode.transitions
		elif isinstance(dataset[0], Transition):
			transitions = list(cast(List[Transition], dataset))
		else:
			raise ValueError(f"invalid dataset type: {type(dataset)}")

		# check action space
		if self.get_action_type() == ActionSpace.BOTH:
			pass
		elif transitions[0].is_discrete:
			assert (
				self.get_action_type() == ActionSpace.DISCRETE
			), DISCRETE_ACTION_SPACE_MISMATCH_ERROR
		else:
			assert (
				self.get_action_type() == ActionSpace.CONTINUOUS
			), CONTINUOUS_ACTION_SPACE_MISMATCH_ERROR

		iterator: TransitionIterator = RoundIterator(
			transitions,
			batch_size=self._batch_size,
			n_steps=self._n_steps,
			gamma=self._gamma,
			n_frames=self._n_frames,
			real_ratio=self._real_ratio,
			generated_maxlen=self._generated_maxlen,
			shuffle=shuffle,
		)

		# initialize scaler
		if self._scaler:
			LOG.debug("Fitting scaler...", scaler=self._scaler.get_type())
			self._scaler.fit(transitions)

		# initialize action scaler
		if self._action_scaler:
			LOG.debug(
				"Fitting action scaler...",
				action_scaler=self._action_scaler.get_type(),
			)
			self._action_scaler.fit(transitions)

		# initialize reward scaler
		if self._reward_scaler:
			LOG.debug(
				"Fitting reward scaler...",
				reward_scaler=self._reward_scaler.get_type(),
			)
			self._reward_scaler.fit(transitions)

		# instantiate implementation
		if self._impl is None:
			LOG.debug("Building models...")
			transition = iterator.transitions[0]
			action_size = transition.get_action_size()
			observation_shape = tuple(transition.get_observation_shape())
			self.create_impl(
				self._process_observation_shape(observation_shape), action_size
			)
			LOG.debug("Models have been built.")
		else:
			LOG.warning("Skip building models since they're already built.")

		assert self._impl is not None, IMPL_NOT_INITIALIZED_ERROR

		# pretraining on z network
		for epoch in range(1, n_epochs + 1):
			range_gen = tqdm(
				range(len(iterator)),
				disable=not show_progress,
				desc=f"Epoch {int(epoch)}/{n_epochs}",
			)

			iterator.reset()

			for itr in range_gen:
				batch = next(iterator)
				loss = self._impl.update_z_net(batch)

	def fit(
		self,
		dataset: Union[List[Episode], List[Transition], MDPDataset],
		n_epochs: Optional[int] = None,
		n_steps: Optional[int] = None,
		n_steps_per_epoch: int = 10000,
		save_metrics: bool = True,
		experiment_name: Optional[str] = None,
		with_timestamp: bool = True,
		logdir: str = "d3rlpy_logs",
		verbose: bool = True,
		show_progress: bool = True,
		tensorboard_dir: Optional[str] = None,
		eval_episodes: Optional[List[Episode]] = None,
		save_interval: int = 1,
		scorers: Optional[
			Dict[str, Callable[[Any, List[Episode]], float]]
		] = None,
		shuffle: bool = True,
		callback: Optional[Callable[["LearnableBase", int, int], None]] = None,
		# extra params
		pretrain_z_net: bool = False,
		pretrain_n_epochs: int = 50
	) -> List[Tuple[int, Dict[str, float]]]:
		
		# TODO: later on to refactor it into pretty forms (hard to modify d3rlpy framework)
		if pretrain_z_net:
			self._pretrain_z_net(
				dataset=dataset, 
				shuffle=shuffle, 
				show_progress=show_progress,
				n_epochs=pretrain_n_epochs)

		# start the real training
		return super().fit(
			dataset=dataset,
			n_epochs=n_epochs,
			n_steps=n_steps,
			n_steps_per_epoch=n_steps_per_epoch,
			save_metrics=save_metrics,
			experiment_name=experiment_name,
			with_timestamp=with_timestamp,
			logdir=logdir,
			verbose=verbose,
			show_progress=show_progress,
			tensorboard_dir=tensorboard_dir,
			eval_episodes=eval_episodes,
			save_interval=save_interval,
			scorers=scorers,
			shuffle=shuffle,
			callback=callback
		)


	def predict(self, x: Union[np.ndarray, List[Any]]) -> np.ndarray:
		return super().predict(pad_obs(x))


	def predict_value(
		self,
		x: Union[np.ndarray, List[Any]],
		action: Union[np.ndarray, List[Any]],
		with_std: bool = False,
	) -> Union[np.ndarray, Tuple[np.ndarray, np.ndarray]]:
		return super().predict_value(pad_obs(x), action, with_std)


	def sample_action(self, x: Union[np.ndarray, List[Any]]) -> np.ndarray:
		return super().sample_action(pad_obs(x))


	def get_adaptive_weights(self, obs: np.ndarray) -> np.ndarray:
		assert self._impl is not None
		
		with torch.no_grad():
			_, normalized_policy_weights = self._impl.get_adaptive_weights(
				torch.FloatTensor(pad_obs(obs)), training=False)
			return normalized_policy_weights.numpy()


class AdaptiveCQLImpl(PaddedObsCQLImpl):
	_beta: float
	_logging_policies: List[LoggingPolicy]
	_z_net: Optional[nn.Module]
	_z_optim: Optional[optim.Optimizer]
	_q_under_z_point_estimate: bool


	def __init__(
		self,
		observation_shape: Sequence[int],
		action_size: int,
		actor_learning_rate: float,
		critic_learning_rate: float,
		temp_learning_rate: float,
		alpha_learning_rate: float,
		actor_optim_factory: OptimizerFactory,
		critic_optim_factory: OptimizerFactory,
		temp_optim_factory: OptimizerFactory,
		alpha_optim_factory: OptimizerFactory,
		actor_encoder_factory: EncoderFactory,
		critic_encoder_factory: EncoderFactory,
		q_func_factory: QFunctionFactory,
		gamma: float,
		tau: float,
		n_critics: int,
		initial_temperature: float,
		initial_alpha: float,
		alpha_threshold: float,
		conservative_weight: float,
		n_action_samples: int,
		soft_q_backup: bool,
		use_gpu: Optional[Device],
		scaler: Optional[Scaler],
		action_scaler: Optional[ActionScaler],
		reward_scaler: Optional[RewardScaler],
		# adaptive params
		beta: float,
		logging_policies: List[LoggingPolicy],
		q_under_z_point_estimate: bool,
		z_learning_rate: float
	):
		super().__init__(
			observation_shape=observation_shape,
			action_size=action_size,
			actor_learning_rate=actor_learning_rate,
			alpha_learning_rate=alpha_learning_rate,
			critic_learning_rate=critic_learning_rate,
			temp_learning_rate=temp_learning_rate,
			alpha_optim_factory=alpha_optim_factory,
			actor_optim_factory=actor_optim_factory,
			critic_optim_factory=critic_optim_factory,
			temp_optim_factory=temp_optim_factory,
			actor_encoder_factory=actor_encoder_factory,
			critic_encoder_factory=critic_encoder_factory,
			q_func_factory=q_func_factory,
			gamma=gamma,
			tau=tau,
			n_critics=n_critics,
			initial_temperature=initial_temperature,
			initial_alpha=initial_alpha,
			alpha_threshold=alpha_threshold,
			conservative_weight=conservative_weight,
			n_action_samples=n_action_samples,
			soft_q_backup=soft_q_backup,
			use_gpu=use_gpu,
			scaler=scaler,
			action_scaler=action_scaler,
			reward_scaler=reward_scaler,
		)

		self._beta = beta
		self._logging_policies = logging_policies
		self._q_under_z_point_estimate = q_under_z_point_estimate
		self._z_learning_rate = z_learning_rate
		self._z_loss = nn.CrossEntropyLoss()


	def build(self) -> None:
		self._build_z_net()
		super().build()
		self._build_z_net_optim()


	def _build_z_net(self) -> None:
		# use the same encoder as the critic
		encoder = self._critic_encoder_factory.create(self._observation_shape)

		# output raw (unnormalized) weights because we will use BCE later
		self._z_net = nn.Sequential(
			cast(nn.Module, encoder),
			nn.Linear(encoder.get_feature_size(), 128),
			nn.ReLU(),
			nn.Linear(128, 128),
			nn.ReLU(),
			nn.Linear(128, len(self._logging_policies)))


	def _build_z_net_optim(self) -> None:
		assert self._z_net is not None

		# use the same lr as the critic
		self._z_optim = optim.Adam(
			self._z_net.parameters(), lr=self._z_learning_rate)


	def get_adaptive_weights(
		self, 
		obs_t: torch.Tensor, 
		training: bool = True
	) -> Tuple[torch.Tensor, torch.Tensor]:
		assert self._z_net is not None
		self._z_net.train(training)

		unnormalized_policy_weights = self._z_net(obs_t)
		normalized_policy_weights = F.softmax(unnormalized_policy_weights, dim=-1)

		self._z_net.train(True)
		return unnormalized_policy_weights, normalized_policy_weights


	def _compute_deterministic_q_under_z(
		self, 
		obs_t: torch.Tensor,
		z_weights: torch.Tensor
	) -> torch.Tensor:
		assert self._q_func is not None

		# logging policy prediction for current states, list of normals
		obs_t_no_padding = obs_t[...,:-1]
		logging_policy_dist = [
			policy.get_action_pdf(obs_t_no_padding) for policy in self._logging_policies]

		# shape (N, batch, action_size), we use mean as the actions
		mean_actions = torch.stack([
			dist.mean for dist in logging_policy_dist
		])

		# get their q values
		stacked_obs = obs_t.repeat(len(self._logging_policies), 1)
		stacked_act = torch.tanh(mean_actions.reshape(-1, self._action_size))
		q_values = self._q_func(stacked_obs, stacked_act)

		# shape (N, B) -> (B, N)
		expected_q = q_values.reshape(len(self._logging_policies), -1).permute(1, 0)

		return (expected_q * z_weights).sum(-1, keepdim=True)


	def _compute_expected_q_under_z(
		self, 
		obs_t: torch.Tensor,
		z_weights: torch.Tensor
	) -> torch.Tensor:
		assert self._q_func is not None

		# logging policy prediction for current states, list of normals
		obs_t_no_padding = obs_t[...,:-1]
		logging_policy_dist = [
			policy.get_action_pdf(obs_t_no_padding) for policy in self._logging_policies]

		# shape (N, samples, batch, action_size)
		sampled_actions = torch.stack([
			dist.sample((self._n_action_samples,)) for dist in logging_policy_dist
		])

		# get their q values
		stacked_obs = obs_t.repeat(len(self._logging_policies) * self._n_action_samples, 1)
		stacked_act = torch.tanh(sampled_actions.reshape(-1, self._action_size))
		q_values = self._q_func(stacked_obs, stacked_act)

		# shape (N, samples, B)
		q_values = q_values.reshape(len(self._logging_policies), self._n_action_samples, -1)

		# expected q values under each logging policy (B, N)
		expected_q = q_values.permute(2, 0, 1).mean(-1)

		return (expected_q * z_weights).sum(-1, keepdim=True)


	@no_type_check
	@train_api
	@torch_api()
	def update_critic(self, batch: TorchMiniBatch) -> np.ndarray:
		""" We update our z net in the critic loop as it is part of the conservative critic """
		assert self._critic_optim is not None
		assert self._z_optim is not None

		self._critic_optim.zero_grad()
		self._z_optim.zero_grad()

		q_tpn = self.compute_target(batch)

		loss = self.compute_critic_loss(batch, q_tpn)

		loss.backward()
		self._critic_optim.step()
		self._z_optim.step()

		return loss.cpu().detach().numpy()


	def compute_critic_loss(
		self, batch: TorchMiniBatch, q_tpn: torch.Tensor
	) -> torch.Tensor:
		assert self._q_func is not None

		obs_zero_padded, source = unpack_obs(batch.observations)
		next_obs_zero_padded, _ = unpack_obs(batch.next_observations)

		loss = self._q_func.compute_error(
			observations=obs_zero_padded,
			actions=batch.actions,
			rewards=batch.rewards,
			target=q_tpn,
			terminals=batch.terminals,
			gamma=self._gamma**batch.n_steps,
		)

		# set include divergence = true to make sure we regularize z net
		conservative_loss = self._compute_conservative_loss(
			obs_t=obs_zero_padded, 
			src_t=source, 
			act_t=batch.actions,
			obs_tp1=next_obs_zero_padded,
			include_divergence=True
		)
		
		return loss + conservative_loss


	def _compute_conservative_loss(
		self,
		obs_t: torch.Tensor, 
		src_t: torch.Tensor, 
		act_t: torch.Tensor,
		obs_tp1: torch.Tensor,
		# whether the returned loss includes the divergence term
		include_divergence: bool = False
	) -> torch.Tensor:

		assert self._policy is not None
		assert self._q_func is not None
		assert self._log_alpha is not None

		policy_values_t = self._compute_policy_is_values(obs_t, obs_t)
		policy_values_tp1 = self._compute_policy_is_values(obs_tp1, obs_t)
		random_values = self._compute_random_is_values(obs_t)

		# compute logsumexp
		# (n critics, batch, 3 * n samples) -> (n critics, batch, 1)
		target_values = torch.cat(
			[policy_values_t, policy_values_tp1, random_values], dim=2
		)
		logsumexp = torch.logsumexp(target_values, dim=2, keepdim=True)

		# adaptive network that outputs our belief on which policy to stay close to at state s
		unnormalized_policy_weights, normalized_policy_weights = self.get_adaptive_weights(obs_t)

		# estimate expected Q values under the z distribution
		if self._q_under_z_point_estimate:
			expected_q_under_z = self._compute_deterministic_q_under_z(obs_t, normalized_policy_weights)
		else:
			expected_q_under_z = self._compute_expected_q_under_z(obs_t, normalized_policy_weights)

		loss = logsumexp.mean(dim=0).mean() - expected_q_under_z.mean()
		scaled_loss = self._conservative_weight * loss

		# clip for stability
		clipped_alpha = self._log_alpha().exp().clamp(0, 1e6)[0][0]

		final_loss = clipped_alpha * (scaled_loss - self._alpha_threshold)

		# compute divergence if needed
		if include_divergence:
			final_loss += self._beta * self._z_loss(unnormalized_policy_weights, src_t.squeeze(-1).long())

		return final_loss


class AdaptiveCQL(PaddedObsCQL):
	_beta: float
	_impl: Optional[AdaptiveCQLImpl]
	_logging_policy_names: List[str]
	_logging_policy_dir: str
	_q_under_z_point_estimate: bool


	def __init__(
		self,
		*,
		actor_learning_rate: float = 1e-4,
		critic_learning_rate: float = 3e-4,
		temp_learning_rate: float = 1e-4,
		alpha_learning_rate: float = 1e-4,
		actor_optim_factory: OptimizerFactory = AdamFactory(),
		critic_optim_factory: OptimizerFactory = AdamFactory(),
		temp_optim_factory: OptimizerFactory = AdamFactory(),
		alpha_optim_factory: OptimizerFactory = AdamFactory(),
		actor_encoder_factory: EncoderArg = "default",
		critic_encoder_factory: EncoderArg = "default",
		q_func_factory: QFuncArg = "mean",
		batch_size: int = 256,
		n_frames: int = 1,
		n_steps: int = 1,
		gamma: float = 0.99,
		tau: float = 0.005,
		n_critics: int = 2,
		initial_temperature: float = 1.0,
		initial_alpha: float = 1.0,
		alpha_threshold: float = 10.0,
		conservative_weight: float = 5.0,
		n_action_samples: int = 10,
		soft_q_backup: bool = False,
		use_gpu: UseGPUArg = False,
		scaler: ScalerArg = None,
		action_scaler: ActionScalerArg = None,
		reward_scaler: RewardScalerArg = None,
		impl: Optional[AdaptiveCQLImpl] = None,
		# extra arguments for Adaptive CQL
		beta: float = 1.0,
		logging_policy_names: List[str],
		logging_policy_dir: str = './d3rlpy_data',
		q_under_z_point_estimate: bool = True,
		z_learning_rate: float = 3e-4,
		**kwargs: Any,
	):
		super().__init__(
			actor_learning_rate=actor_learning_rate,
			critic_learning_rate=critic_learning_rate,
			temp_learning_rate=temp_learning_rate,
			alpha_learning_rate=alpha_learning_rate,
			actor_optim_factory=actor_optim_factory,
			critic_optim_factory=critic_optim_factory,
			temp_optim_factory=temp_optim_factory,
			alpha_optim_factory=alpha_optim_factory,
			actor_encoder_factory=actor_encoder_factory,
			critic_encoder_factory=critic_encoder_factory,
			q_func_factory=q_func_factory,
			batch_size=batch_size,
			n_frames=n_frames,
			n_steps=n_steps,
			gamma=gamma,
			tau=tau,
			n_critics=n_critics,
			initial_temperature=initial_temperature,
			initial_alpha=initial_alpha,
			alpha_threshold=alpha_threshold,
			conservative_weight=conservative_weight,
			n_action_samples=n_action_samples,
			soft_q_backup=soft_q_backup,
			use_gpu=use_gpu,
			scaler=scaler,
			action_scaler=action_scaler,
			reward_scaler=reward_scaler,
			impl=impl,
			**kwargs
		)

		self._beta = beta
		self._logging_policy_names = list(logging_policy_names)
		self._logging_policy_dir = logging_policy_dir
		self._q_under_z_point_estimate = q_under_z_point_estimate
		self._z_learning_rate = z_learning_rate


	def _create_impl(
		self, observation_shape: Sequence[int], action_size: int
	) -> None:
		self._impl = AdaptiveCQLImpl(
			observation_shape=observation_shape,
			action_size=action_size,
			actor_learning_rate=self._actor_learning_rate,
			critic_learning_rate=self._critic_learning_rate,
			temp_learning_rate=self._temp_learning_rate,
			alpha_learning_rate=self._alpha_learning_rate,
			actor_optim_factory=self._actor_optim_factory,
			critic_optim_factory=self._critic_optim_factory,
			temp_optim_factory=self._temp_optim_factory,
			alpha_optim_factory=self._alpha_optim_factory,
			actor_encoder_factory=self._actor_encoder_factory,
			critic_encoder_factory=self._critic_encoder_factory,
			q_func_factory=self._q_func_factory,
			gamma=self._gamma,
			tau=self._tau,
			n_critics=self._n_critics,
			initial_temperature=self._initial_temperature,
			initial_alpha=self._initial_alpha,
			alpha_threshold=self._alpha_threshold,
			conservative_weight=self._conservative_weight,
			n_action_samples=self._n_action_samples,
			soft_q_backup=self._soft_q_backup,
			use_gpu=self._use_gpu,
			scaler=self._scaler,
			action_scaler=self._action_scaler,
			reward_scaler=self._reward_scaler,			
			beta=self._beta,
			logging_policies=self._build_logging_policies(),
			q_under_z_point_estimate=self._q_under_z_point_estimate,
			z_learning_rate=self._z_learning_rate,
		)
		self._impl.build()


	def _build_logging_policies(self) -> List[LoggingPolicy]:
		# use the same device for logging policy
		device = 'cpu'
		if self._use_gpu is not None:
			device = f'cuda:{self._use_gpu.get_id()}'

		return [load_logging_policy(
			policy_name=name, 
			saving_dir=self._logging_policy_dir, 
			device=device) for name in self._logging_policy_names]

