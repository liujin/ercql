from pathlib import Path  # type: ignore
from typing import Any, Callable, Dict, List, Literal, Optional, Tuple, Union

import gym  # type: ignore
import matplotlib  # type: ignore
import matplotlib.pyplot as plt  # type: ignore
import numpy as np
from d3rlpy.dataset import MDPDataset
from rliable import library as rly  # type: ignore
from rliable import metrics, plot_utils  # type: ignore
from sklearn.manifold import TSNE  # type: ignore

from visualization.data_parser import D3rlpyCSVDataParser, Records


def get_min_horizon(algo_to_records: Dict[str, List[Records]]) -> int:
	min_horizon = int(1e9)

	for _, records in algo_to_records.items():
		for record in records:
			min_horizon = min(min_horizon, len(record))

	return min_horizon


def plot_records_list(
		axes: matplotlib.axes.Axes,
		algo_to_records: Dict[str, List[Records]],
		env_name: str,
		value_description: str = 'loss',
		horizon_name: Union[Literal['epochs'], Literal['steps']] = 'epochs',
		**kwargs: Any # arguments to the plot function
	) -> None:
	""" 
		Plot the graph of different algorithms,
		each algorithm contains multiple experiments,
		all experiments are from the same environment
	"""

	# make sure all algorithms have the same number of experiments
	experiment_counts = set([len(data) for data in algo_to_records.values()])
	assert len(experiment_counts) == 1, \
		"All algorithms should have the same number of experiments"

	# truncate horizon (assuming monotonic increasing)
	min_horizon = get_min_horizon(algo_to_records)

	for algo_name in sorted(algo_to_records.keys()):
		algo_records_list = algo_to_records[algo_name]

		horizon = algo_records_list[0].get_data(min_horizon)[horizon_name]
		values = np.array([records.get_data(min_horizon)['values'] for records in algo_records_list])
		value_mean = np.mean(values, axis=0)
		value_std = np.std(values, axis=0)

		axes.plot(horizon, value_mean, label=algo_name, **kwargs)
		axes.fill_between(
			horizon, 
			value_mean - value_std, 
			value_mean + value_std, 
			alpha=0.2, 
			interpolate=True,
			label='_{}_std'.format(algo_name)
		)

	axes.set_title('{}: {} plots of {}\nover {} trials'.format(
		env_name, value_description, horizon_name, next(iter(experiment_counts))))
	axes.set_ylabel(value_description)
	axes.set_xlabel(horizon_name)
	
	axes.legend()


def plot_final_performance(
		algo_to_performance: Dict[str, List[float]],
		env_name: str,
		value_description: str = 'loss',
		**kwargs: Any # arguments to the plot function
	) -> None:
	algorithms = sorted(list(algo_to_performance.keys()))
	score_dict = { k: np.array([algo_to_performance[k]]).transpose() for k in algorithms }

	aggregate_func = lambda x: np.array([
		metrics.aggregate_median(x),
		metrics.aggregate_iqm(x),
		metrics.aggregate_mean(x),
		metrics.aggregate_optimality_gap(x)
	])

	aggregate_scores, aggregate_interval_estimates = rly.get_interval_estimates(
		score_dict, aggregate_func, reps=5000)

	fig, axes = plot_utils.plot_interval_estimates(
		aggregate_scores, 
		aggregate_interval_estimates,
		metric_names = ['Median', 'IQM', 'Mean', 'Optimality Gap'],
		algorithms=algorithms)

	fig.suptitle(value_description)


def plot_final_performance_pi(
		algo_to_performance: Dict[str, List[float]],
		env_name: str,
		value_description: str = 'loss',
		baseline_name: str = 'empty',
		**kwargs: Any # arguments to the plot function
	) -> None:
	algorithms = sorted(list(algo_to_performance.keys()))
	score_dict = { k: np.array([algo_to_performance[k]]).transpose() for k in algorithms }

	pair_map: Dict[str, Tuple[np.ndarray, np.ndarray]] = {}

	for algo in algorithms:
		if algo != baseline_name:
			pair_map[f'{algo},{baseline_name}'] = (
				score_dict[algo],
				score_dict[baseline_name])

	aggregate_scores, aggregate_interval_estimates = rly.get_interval_estimates(
		pair_map, metrics.probability_of_improvement, reps=5000)

	axes = plot_utils.plot_probability_of_improvement(
		aggregate_scores, 
		aggregate_interval_estimates)

	axes.set_title(value_description)


def normalize_score_by_relative_improvement(		
		algo_to_records: Dict[str, List[Records]],
		normalize_baseline: str
	) -> Dict[str, List[Records]]:
	
	epsilon = 1e-9

	assert normalize_baseline in algo_to_records, 'Normalize baseline provided is incorrect'

	# get baseline records and remove from map
	baseline_records = algo_to_records.pop(normalize_baseline)

	# truncate horizon (assuming monotonic increasing)
	min_horizon = get_min_horizon(algo_to_records)

	# normalize all other records w.r.t. baselines
	for algo_name in algo_to_records:
		assert len(algo_to_records[algo_name]) == len(baseline_records), \
			'Baseline records have different length to {}.'.format(algo_name)
		for record_to_normalize, baseline_record in zip(algo_to_records[algo_name], baseline_records):
			raw_values = np.array(record_to_normalize.get_data(min_horizon)['values'])
			baseline_values = np.array(baseline_record.get_data(min_horizon)['values'])
			
			normalized_values = (raw_values - baseline_values) / (np.abs(baseline_values) + epsilon) * 100.0
			record_to_normalize.set_values(normalized_values.tolist())

	return algo_to_records


def normalize_score_by_linear_minmax(
		algo_to_records: Dict[str, List[Records]],
		min_baseline: str,
		max_baseline: str
	) -> Dict[str, List[Records]]:
	
	epsilon = 1e-9

	assert min_baseline in algo_to_records, 'Min baseline provided is incorrect'
	assert max_baseline in algo_to_records, 'Max baseline provided is incorrect'

	min_records = algo_to_records.pop(min_baseline)
	max_records = algo_to_records.pop(max_baseline)

	# truncate horizon (assuming monotonic increasing)
	min_horizon = get_min_horizon(algo_to_records)

	# normalize all other records w.r.t. min and max baselines
	for algo_name in algo_to_records:
		assert len(algo_to_records[algo_name]) == len(min_records), \
			'Min records have different length to {}.'.format(algo_name)
		assert len(algo_to_records[algo_name]) == len(max_records), \
			'Max records have different length to {}.'.format(algo_name)

		for cur_r, min_r, max_r in zip(algo_to_records[algo_name], min_records, max_records):

			raw_values = np.array(cur_r.get_data(min_horizon)['values'])
			min_values = np.array(min_r.get_data(min_horizon)['values'])
			max_values = np.array(max_r.get_data(min_horizon)['values'])
			
			normalized_values = (raw_values - min_values) / (max_values - min_values + epsilon) * 100
			cur_r.set_values(normalized_values.tolist())

	return algo_to_records


def load_records_in_dir(
		log_dir: str,
		value_description: str,
		parse_experiment_name: Callable[[str], Tuple[str, str]] = None
	) -> Dict[str, List[Records]]:
	
	log_dir_path = Path(log_dir)
	assert log_dir_path.is_dir(), "Invalid log dir."
	
	parser = D3rlpyCSVDataParser(parse_experiment_name)
	records_list: List[Records] = []
	
	for sub_dir in log_dir_path.iterdir():
		if sub_dir.is_dir():
			records_list.append(parser.parse(str(sub_dir), value_description=value_description))

	algo_to_records: Dict[str, List[Records]] = {}
	for records in records_list:
		algo_name = records.algo_name
		if algo_name not in algo_to_records:
			algo_to_records[algo_name] = []
		algo_to_records[algo_name].append(records)

	for algo_name in algo_to_records:
		algo_to_records[algo_name] = sorted(algo_to_records[algo_name], key=lambda record: record.trial_name)

	return algo_to_records


def plot_records_in_dir(
		log_dir: str,
		env_name: str,
		value_description: str = 'loss',
		horizon_name: Union[Literal['epochs'], Literal['steps']] = 'epochs',
		normalize_baseline: Optional[Union[str, Tuple[str, str]]] = None,
		parse_experiment_name: Callable[[str], Tuple[str, str]] = None,
		**kwargs
	) -> None:

	algo_to_records	= load_records_in_dir(log_dir, value_description, parse_experiment_name)

	if normalize_baseline is not None:
		if isinstance(normalize_baseline, str):
			algo_to_records = normalize_score_by_relative_improvement(algo_to_records, normalize_baseline)
			value_description += ' (normalized by {})'.format(normalize_baseline)
			plt.gca().axhline(y=0.0, color='gray', linestyle='--', label=normalize_baseline)
		elif isinstance(normalize_baseline, tuple):
			algo_to_records = normalize_score_by_linear_minmax(algo_to_records, *normalize_baseline)
			value_description += ' (normalized by {})'.format(normalize_baseline)
			plt.gca().axhline(y=0.0, color='gray', linestyle='--', label=normalize_baseline[0])
			plt.gca().axhline(y=1.0, color='gray', linestyle='--', label=normalize_baseline[1])

	plot_records_list(
		axes=plt.gca(), 
		algo_to_records=algo_to_records, 
		env_name=env_name,
		value_description=value_description, 
		horizon_name=horizon_name, 
		**kwargs
	)

	plt.show()


def print_final_performance_in_dir(
		log_dir: str,
		value_description: str = 'loss',
		# which step (or epoch) to plot
		horizon_idx: int = -1,
		parse_experiment_name: Callable[[str], Tuple[str, str]] = None,
		sort_based_on_performance: bool = True
	) -> None:
	
	assert horizon_idx >= -1, "Invalid horizon idx value"
	algo_to_records	= load_records_in_dir(log_dir, value_description, parse_experiment_name)	

	min_horizon = get_min_horizon(algo_to_records)
	plot_horizon = min_horizon - 1 if horizon_idx == -1 else min(min_horizon - 1, horizon_idx)

	final_performance = []
	for algo_name, records in algo_to_records.items():
		values = np.array([record.get_data()['values'][plot_horizon] for record in records])
		final_performance.append((values.mean(), values.std(), algo_name))

	if sort_based_on_performance:
		final_performance = sorted(final_performance, reverse=True)

	print(f'Final performance (sorted={sort_based_on_performance}):')
	for performance in final_performance:
		mean, std, algo_name = performance
		print(f'{algo_name}: {mean:.2f}±{std:.2f}')


def plot_final_performance_in_dir(
		log_dir: str,
		env_name: str,
		value_description: str = 'loss',
		# which step (or epoch) to plot
		horizon_idx: int = -1,
		parse_experiment_name: Callable[[str], Tuple[str, str]] = None,
		baseline_name: str = 'empty',
		**kwargs
	) -> None:
	
	assert horizon_idx >= -1, "Invalid horizon idx value"
	algo_to_records	= load_records_in_dir(log_dir, value_description, parse_experiment_name)	

	# algo_to_records = normalize_score_by_linear_minmax(algo_to_records, 'empty', 'behaviour')

	min_horizon = get_min_horizon(algo_to_records)
	plot_horizon = min_horizon - 1 if horizon_idx == -1 else min(min_horizon - 1, horizon_idx)

	if horizon_idx >= 0 and plot_horizon != horizon_idx:
		print("Given horizon idx value is too large for the data")

	algo_to_performance: Dict[str, List[float]] = {}
	for algo_name in algo_to_records:
		algo_to_performance[algo_name] = []
		for record in algo_to_records[algo_name]:
			algo_to_performance[algo_name].append(record.get_data()["values"][plot_horizon])

	plot_final_performance(
		algo_to_performance=algo_to_performance,
		env_name=env_name,
		value_description='{}@{}'.format(value_description, 'final' if min_horizon == -1 else plot_horizon),
		**kwargs
	)
	
	plt.tight_layout()
	plt.show()

	plot_final_performance_pi(
		algo_to_performance=algo_to_performance,
		env_name=env_name,
		value_description='{}: probability of improvement'.format(value_description),
		baseline_name=baseline_name,
		**kwargs
	)

	plt.tight_layout()
	plt.show()


def visualize_state_space(
	dataset_names: List[str],
	env_name: str,
	dataset_dir: str = './d3rlpy_data',
	subsample_size: Optional[int] = 1000
) -> None:
	data = []
	data_sizes = [0]

	for name in dataset_names:
		mdp_dataset = MDPDataset.load(f'{dataset_dir}/{env_name}_{name}.h5')
		obs = mdp_dataset.observations
		if subsample_size:
			obs = obs[np.random.choice(obs.shape[0], subsample_size)]
		data_sizes.append(data_sizes[-1] + obs.shape[0])
		data.append(obs)

	stacked_data = np.vstack(data)
	data_embedded = TSNE(n_components=2, learning_rate='auto', init='random').fit_transform(stacked_data)

	for idx, name in enumerate(dataset_names):
		xy = data_embedded[data_sizes[idx]:data_sizes[idx + 1]]
		x = xy[:, 0]
		y = xy[:, 1]
		plt.scatter(x, y, label=name)

	plt.legend()
	plt.show()
