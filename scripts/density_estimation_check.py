import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.optim import Adam

import numpy as np
import matplotlib.pyplot as plt


p1 = np.random.randn(100, 2) * 1.3 + 1
p2 = np.random.randn(100, 2) * 0.5 - 1
x, y = np.meshgrid(np.linspace(-5, 5), np.linspace(-5, 5))
samples = np.vstack([x.ravel(), y.ravel()]).transpose()


def plot(r, epoch):
	ax1 = plt.subplot(121, aspect='equal')
	ax1.scatter(p1[:, 0], p1[:, 1])
	ax1.scatter(p2[:, 0], p2[:, 1])
	ax1.set_title('Two-Class Training Data')

	ax2 = plt.subplot(122, aspect='equal', sharex=ax1, sharey=ax1)
	ax2.set_title(f'Class 1 Pred at Epoch: {epoch}')
	im = ax2.scatter(samples[:, 0], samples[:, 1], c=r)
	
	plt.colorbar(im, ax=[ax1, ax2], orientation='horizontal', location='bottom')
	plt.show()


ce_loss = nn.CrossEntropyLoss()
net = nn.Sequential(
	nn.Linear(2, 64),
	nn.ReLU(),
	nn.Linear(64, 64),
	nn.ReLU(),
	nn.Linear(64, 2))
optim = Adam(net.parameters(), lr=1e-3, weight_decay=0.05)


def compute_loss(pred, y):
	return ce_loss(pred, y)


for epoch in range(101):
	X = torch.FloatTensor(np.vstack([p1, p2]))
	y = torch.LongTensor(np.hstack([np.zeros(100), np.ones(100)]))

	optim.zero_grad()
	pred = net(X)
	loss = compute_loss(pred, y)
	loss.backward()
	optim.step()

	net.eval()
	with torch.no_grad():
		o = F.softmax(net(torch.FloatTensor(samples)), dim=-1)
		r = o.numpy()[:, 0]

		print(loss.item())

		if epoch % 10 == 0:
			plot(r, epoch)

	net.train()
