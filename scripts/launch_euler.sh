# !/bin/bash

E_TIME=4:00
MEM=2048
N_CORE=20

bsub -W ${E_TIME} -n ${N_CORE} -R "rusage[mem=${MEM}]" "python code/run.py"
