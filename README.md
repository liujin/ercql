# Semester Project

## Authors
Jingyu Liu

Scott Sussex

## Intro
This repo contains a general framework for running the following types of experiments:

1. Single-stage offline reinforcement learning training (the traditional setting)
2. Two-stage offline reinforcement learning training (a normal offline stage followed by an online stage and finally a second offline stage)

Each configured "experiment" (specifying all necessary information of a strategy) is called a **pipeline**, consisting of various aspects and params of a training process. 

## Training Paradigm
All training configs are in `code/conf` with the following structures:
```
.
├── config.yaml <default config file>
├── pipeline_gen.py <an auto script for generating sweeping param experiments>
├── agents
│   ├── <configs for specifying implementations of agents>
│   └── ...
├── experiments
│   ├── single_stage
│   │   ├── <configs (env, datasize, seeds, etc) for single-stage training>
│   │   └── ...
│   └── two_stage (same structure as single_stage)
├── pipelines
│   ├── single_stage
│   │   ├── instances
│   │   ├── <configs for pipeline groups: the list of pipelines we want to run for the experiment>
│   │   └── ...
│   └── two_stage (same structure as single_stage)
```

A single-stage pipeline (instance) consists of (major components):
1. offline_data
2. agent

A two-stage pipeline (instance) consists of (major components):
1. offline_data
2. first_stage_agent
3. online_strategy
4. second_stage_agent

The interfaces for these components are in `code/core`.

## Run the Code
First install the required packages (gym might need to upgrade to 0.21.0.):
```
conda env create -f environment.yaml
```
Then run the experiment with specified config (no yaml extension):
```
python code/run.py <--config-name specified_config_name>
```

## Plot the Results
All the experiment results are stored in the logging_dir, run:
```
python code/plot.py
    --print -p <only print the final results, no plot>
    --experiment-name -n <the experiment name in the config file>
    --env-name -e <the environment name>
    --value-description -v <which metrics to plot, examples including rollout_returns, td_errors, init_state_value_estimate>
    --horizon-idx -H <the max horizon to plot, truncating all remaining data>
```
